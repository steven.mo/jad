package com.jad.wx.mp.resp;

import java.io.Serializable;

public class CommonResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected String errcode;//响应码
	protected String errmsg;//响应信息
	
	public String getErrcode() {
		return errcode;
	}
	public void setErrcode(String errcode) {
		this.errcode = errcode;
	}
	public String getErrmsg() {
		return errmsg;
	}
	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}
}
