package com.jad.wx.mp.resp;

import java.io.Serializable;
import java.util.List;

public class NewsArticlesMsgResp  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<NewsArticleItemsMsgResp>item;

	public List<NewsArticleItemsMsgResp> getItem() {
		return item;
	}

	public void setItem(List<NewsArticleItemsMsgResp> item) {
		this.item = item;
	}


}
