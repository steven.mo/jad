package com.jad.wx.mp.resp;

import java.io.Serializable;

/**
 * 微信组
 * @author hechuan
 *
 */
public class WxmpGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id ;//	分组id，由微信分配
	private String name ;//	分组名字，UTF8编码 
	private String count;// 	分组内用户数量 
	
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
