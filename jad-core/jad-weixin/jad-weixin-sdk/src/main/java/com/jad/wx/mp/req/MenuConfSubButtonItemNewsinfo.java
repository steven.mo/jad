package com.jad.wx.mp.req;

import java.io.Serializable;
import java.util.List;

public class MenuConfSubButtonItemNewsinfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<MenuConfSubButtonItemNewsinfoItem>list;

	public List<MenuConfSubButtonItemNewsinfoItem> getList() {
		return list;
	}

	public void setList(List<MenuConfSubButtonItemNewsinfoItem> list) {
		this.list = list;
	}



}
