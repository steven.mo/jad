package com.jad.wx.mp.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 客服管理返回码
 * @author hechuan
 *
 */
public class KfRespCode {

	public static Map<String ,KfRespCode>ALL=new HashMap<String,KfRespCode>();
	
	public static KfRespCode CODE_0=new KfRespCode("0","成功");
	public static KfRespCode CODE_65400=new KfRespCode("65400","API不可用，即没有开通/升级到新版客服功能");
	public static KfRespCode CODE_65401=new KfRespCode("65401","无效客服帐号");
	public static KfRespCode CODE_65403=new KfRespCode("65403","客服昵称不合法");
	public static KfRespCode CODE_65404=new KfRespCode("65404","客服帐号不合法");
	public static KfRespCode CODE_65405=new KfRespCode("65405","帐号数目已达到上限，不能继续添加");
	public static KfRespCode CODE_65406=new KfRespCode("65406","已经存在的客服帐号");
	public static KfRespCode CODE_65407=new KfRespCode("65407","邀请对象已经是该公众号客服");
	public static KfRespCode CODE_65408=new KfRespCode("65408","本公众号已经有一个邀请给该微信");
	public static KfRespCode CODE_65409=new KfRespCode("65409","无效的微信号");
	public static KfRespCode CODE_65410=new KfRespCode("65410","邀请对象绑定公众号客服数达到上限（目前每个微信号可以绑定5个公众号客服帐号）");
	public static KfRespCode CODE_65411=new KfRespCode("65411","该帐号已经有一个等待确认的邀请，不能重复邀请");
	public static KfRespCode CODE_65412=new KfRespCode("65412","该帐号已经绑定微信号，不能进行邀请");
	public static KfRespCode CODE_40005=new KfRespCode("40005","不支持的媒体类型");
	public static KfRespCode CODE_40009=new KfRespCode("40009","媒体文件长度不合法");
	
	public static KfRespCode valueOf(String code){
		
		return ALL.get(code);
	}
	
	public KfRespCode(String code,String msg){
		this.code=code;this.msg=msg;
		ALL.put(code, this);
	}
	
	private String code;
	private String msg;

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
