package com.jad.wx.mp.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.json.JsonMapper;
import com.jad.wx.mp.constant.KfRespCode;
import com.jad.wx.mp.constant.MpRespCode;
import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.CommonResp;

public class AbstractWxServiceImpl {
	
	private static Logger logger=LoggerFactory.getLogger(AbstractWxServiceImpl.class);
	
	protected void checkSuccess(CommonResp resp)throws WeixinMpException{
		
		if(resp==null || StringUtils.isBlank(resp.getErrcode())){
			return ;
		}
		
		if(MpRespCode.CODE_0.getCode().equals(resp.getErrcode())){
			return ;
		}
		
		MpRespCode c=MpRespCode.valueOf(resp.getErrcode());
		
		if(c!=null){
			logger.error("checkSuccess fail,resp:"+JsonMapper.toJsonString(resp));
			throw new WeixinMpException(c.getMsg());
		}
		
		KfRespCode kfc=KfRespCode.valueOf(resp.getErrcode());
		if(kfc!=null){
			logger.error("checkSuccess fail,resp:"+JsonMapper.toJsonString(resp));
			throw new WeixinMpException(kfc.getMsg());
		}
		
		logger.error("checkSuccess fail,resp:"+JsonMapper.toJsonString(resp));
		throw new WeixinMpException(resp.getErrmsg()+(resp.getErrcode()));
		
		
	}

}
