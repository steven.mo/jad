package com.jad.wx.mp.resp;

/**
 * 回复音乐消息
 * @author hechuan
 *
 */
public class MusicMsgResp extends MsgReplyResp{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String title 	;//否 	音乐标题
	private String description ;//	否 	音乐描述
	private String musicURL ;//	否 	音乐链接
	private String hqMusicUrl 	;//否 	高质量音乐链接，WIFI环境优先使用该链接播放音乐
	private String thumbMediaId ;//	否 	缩略图的媒体id，通过素材管理接口上传多媒体文件，得到的id
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMusicURL() {
		return musicURL;
	}
	public void setMusicURL(String musicURL) {
		this.musicURL = musicURL;
	}
	public String getHqMusicUrl() {
		return hqMusicUrl;
	}
	public void setHqMusicUrl(String hqMusicUrl) {
		this.hqMusicUrl = hqMusicUrl;
	}
	public String getThumbMediaId() {
		return thumbMediaId;
	}
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

}
