package com.jad.wx.mp.req;

import java.io.Serializable;

/**
 * 修改永久素材
 * @author hechuan
 *
 */
public class UpdateAttachmentReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String media_id 	;//是 	要修改的图文消息的id
	private String index ;//	是 	要更新的文章在图文消息中的位置（多图文消息时，此字段才有意义），第一篇为0 
	private UpdateAttachmentArticleReq articles;
	
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public UpdateAttachmentArticleReq getArticles() {
		return articles;
	}
	public void setArticles(UpdateAttachmentArticleReq articles) {
		this.articles = articles;
	}
	
	
}
