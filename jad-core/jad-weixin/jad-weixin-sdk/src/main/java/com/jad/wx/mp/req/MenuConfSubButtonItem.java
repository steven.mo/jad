package com.jad.wx.mp.req;

import java.io.Serializable;

public class MenuConfSubButtonItem implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String type 	;
	private String name 	;
	private String key 	;
	private String url 	;
	private String value 	;
	private MenuConfSubButtonItemNewsinfo news_info;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public MenuConfSubButtonItemNewsinfo getNews_info() {
		return news_info;
	}
	public void setNews_info(MenuConfSubButtonItemNewsinfo news_info) {
		this.news_info = news_info;
	}


}
