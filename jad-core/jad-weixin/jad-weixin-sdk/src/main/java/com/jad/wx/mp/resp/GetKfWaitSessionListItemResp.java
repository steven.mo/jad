package com.jad.wx.mp.resp;

import java.io.Serializable;

public class GetKfWaitSessionListItemResp implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String openid;//	粉丝的openid
	private String latest_time;//	粉丝的最后一条消息的时间
	
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getLatest_time() {
		return latest_time;
	}
	public void setLatest_time(String latest_time) {
		this.latest_time = latest_time;
	}
	

	
	

}
