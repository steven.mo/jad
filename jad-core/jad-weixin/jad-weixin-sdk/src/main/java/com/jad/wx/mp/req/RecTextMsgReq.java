package com.jad.wx.mp.req;

/**
 * 接收文本消息
 * @author hechuan
 *  
 */
public class RecTextMsgReq  extends RecSimpleMsgReq{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String content 	;//文本消息内容
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

}
