package com.jad.wx.mp.resp;

import java.util.List;

/**
 * 获取客服聊天记录
 * @author hechuan
 *
 */
public class GetKfmsglistResp extends CommonResp {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int number;
	private long msgid;
	private List<GetKfmsglistItemResp>recordlist;
	
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public List<GetKfmsglistItemResp> getRecordlist() {
		return recordlist;
	}
	public void setRecordlist(List<GetKfmsglistItemResp> recordlist) {
		this.recordlist = recordlist;
	}
	public long getMsgid() {
		return msgid;
	}
	public void setMsgid(long msgid) {
		this.msgid = msgid;
	}
	
	
	
	

}
