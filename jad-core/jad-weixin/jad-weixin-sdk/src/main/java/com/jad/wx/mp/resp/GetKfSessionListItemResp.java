package com.jad.wx.mp.resp;

import java.io.Serializable;

public class GetKfSessionListItemResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String createtime;//"" : 123456789,
	private String openid;//"" : "OPENID"
	
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	

}
