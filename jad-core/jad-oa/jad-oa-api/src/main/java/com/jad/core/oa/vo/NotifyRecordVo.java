package com.jad.core.oa.vo;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.vo.BaseVo;

public class NotifyRecordVo extends BaseVo<String> {
	
	private static final long serialVersionUID = 1L;
	
	private NotifyVo notify;		// 通知通告ID
//	private UserVo user;		// 接受人
	private String readFlag;		// 阅读标记（0：未读；1：已读）
	private Date readDate;		// 阅读时间
	
	public NotifyRecordVo() {
		super();
	}

	public NotifyRecordVo(String id){
		super(id);
	}
	
	
	public NotifyVo getNotify() {
		return notify;
	}

	public void setNotify(NotifyVo notify) {
		this.notify = notify;
	}

	@Length(min=0, max=1, message="阅读标记（0：未读；1：已读）长度必须介于 0 和 1 之间")
	public String getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(String readFlag) {
		this.readFlag = readFlag;
	}
	
	public Date getReadDate() {
		return readDate;
	}

	public void setReadDate(Date readDate) {
		this.readDate = readDate;
	}
	

	
}
