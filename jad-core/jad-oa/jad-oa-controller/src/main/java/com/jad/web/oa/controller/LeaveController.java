/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.jad.web.oa.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jad.web.mvc.BaseController;

/**
 * 请假Controller
 * @author liuj
 * @version 2013-04-05
 */
@Controller
@RequestMapping(value = "${adminPath}/oa/leave")
public class LeaveController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(getClass());

//	@Autowired
//	protected LeaveService leaveService;

//	@Autowired
//	protected ActTaskService taskService;
	
//	@Autowired
//	protected SystemService systemService;
	

//	@RequiresPermissions("oa:leave:view")
//	@RequestMapping(value = {"form"})
//	public String form(Leave leave, Model model) {
//		model.addAttribute("leave", leave);
//		return "modules/oa/leaveForm";
//	}
//
//	/**
//	 * 启动请假流程
//	 * @param leave	
//	 */
//	@RequiresPermissions("oa:leave:edit")
//	@RequestMapping(value = "save", method = RequestMethod.POST)
//	public String save(Leave leave, RedirectAttributes redirectAttributes) {
//		try {
//			Map<String, Object> variables = Maps.newHashMap();
//			leaveService.save(leave, variables);
//			addMessage(redirectAttributes, "流程已启动，流程ID：" + leave.getProcessInstanceId());
//		} catch (Exception e) {
//			logger.error("启动请假流程失败：", e);
//			addMessage(redirectAttributes, "系统内部错误！");
//		}
//		return "redirect:" + adminPath + "/oa/leave/form";
//	}
//
//	/**
//	 * 任务列表
//	 * @param leave	
//	 */
//	@RequiresPermissions("oa:leave:view")
//	@RequestMapping(value = {"list/task",""})
//	public String taskList(HttpSession session, Model model) {
////		String userId = systemService.getUser().getLoginName();//ObjectUtils.toString(UserUtils.getUser().getId());
//		String userId = super.getCurrentUserId();
//		List<Leave> results = leaveService.findTodoTasks(userId);
//		model.addAttribute("leaves", results);
//		return "modules/oa/leaveTask";
//	}
//
//	/**
//	 * 读取所有流程
//	 * @return
//	 */
//	@RequiresPermissions("oa:leave:view")
//	@RequestMapping(value = {"list"})
//	public String list(Leave leave, HttpServletRequest request, HttpServletResponse response, Model model) {
//        Page<Leave> page = leaveService.find(getPage(request, response), leave); 
//        model.addAttribute("page", page);
//		return "modules/oa/leaveList";
//	}
//	
//	/**
//	 * 读取详细数据
//	 * @param id
//	 * @return
//	 */
//	@RequestMapping(value = "detail/{id}")
//	@ResponseBody
//	public String getLeave(@PathVariable("id") String id) {
//		Leave leave = leaveService.get(id);
//		return JsonMapper.getInstance().toJson(leave);
//	}
//
//	/**
//	 * 读取详细数据
//	 * @param id
//	 * @return
//	 */
//	@RequestMapping(value = "detail-with-vars/{id}/{taskId}")
//	@ResponseBody
//	public String getLeaveWithVars(@PathVariable("id") String id, @PathVariable("taskId") String taskId) {
//		Leave leave = leaveService.get(id);
//		Map<String, Object> variables = taskService.getVariables(taskId);
//		leave.setVariables(variables);
//		return JsonMapper.getInstance().toJson(leave);
//	}

}
