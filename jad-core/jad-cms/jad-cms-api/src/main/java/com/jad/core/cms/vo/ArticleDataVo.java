/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.sys.vo.MenuVo;

/**
 * 文章内容
 * @author ThinkGem
 * @version 2013-01-15
 */
public class ArticleDataVo extends BaseVo<String> {

	private static final long serialVersionUID = 1L;
	
	@CURD(label="内容")
	private String content;	// 内容
	
	@CURD(label="来源")
	private String copyfrom;// 来源
	
	@CURD(label="相关文章")
	private String relation;// 相关文章
	
	@CURD(label="是否允许评论")
	private String allowComment;// 是否允许评论

	private ArticleVo article;
	
	public ArticleDataVo() {
		super();
	}
	
	public ArticleDataVo(String id){
		this();
		this.id = id;
	}

	@NotBlank
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Length(min=0, max=255)
	public String getCopyfrom() {
		return copyfrom;
	}

	public void setCopyfrom(String copyfrom) {
		this.copyfrom = copyfrom;
	}

	@Length(min=0, max=255)
	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	@Length(min=1, max=1)
	public String getAllowComment() {
		return allowComment;
	}

	public void setAllowComment(String allowComment) {
		this.allowComment = allowComment;
	}

	@JsonIgnore
	public ArticleVo getArticle() {
		return article;
	}

	public void setArticle(ArticleVo article) {
		this.article = article;
	}

}