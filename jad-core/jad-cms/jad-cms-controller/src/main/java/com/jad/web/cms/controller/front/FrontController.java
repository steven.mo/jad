/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller.front;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jad.commons.context.Constants;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.cms.qo.ArticleQo;
import com.jad.core.cms.qo.CategoryQo;
import com.jad.core.cms.qo.CommentQo;
import com.jad.core.cms.qo.LinkQo;
import com.jad.core.cms.service.ArticleDataService;
import com.jad.core.cms.service.ArticleService;
import com.jad.core.cms.service.CategoryService;
import com.jad.core.cms.service.CommentService;
import com.jad.core.cms.service.LinkService;
import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.utils.ArticleUtil;
import com.jad.core.cms.utils.CategoryUtil;
import com.jad.core.cms.vo.ArticleVo;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.core.cms.vo.CommentVo;
import com.jad.core.cms.vo.LinkVo;
import com.jad.core.cms.vo.SiteVo;
import com.jad.web.cms.util.CmsApiUtils;
import com.jad.web.mvc.BaseController;

/**
 * 网站Controller
 * @author ThinkGem
 * @version 2013-5-29
 */
@Controller
@RequestMapping(value = "${frontPath}")
public class FrontController extends BaseController{
	
	@Autowired
	private ArticleService articleService;
	@Autowired
	private ArticleDataService articleDataService;
	@Autowired
	private LinkService linkService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private SiteService siteService;
	
	
	@ModelAttribute
	public void init(Model model,HttpServletRequest request) {
		model.addAttribute("contextPath", request.getContextPath());
		model.addAttribute("frontPath", frontPath);
		model.addAttribute("urlSuffix", urlSuffix);
	}
	
	/**
	 * 网站首页
	 */
	@RequestMapping
	public String index(Model model) {
//		Site site = CmsApiUtils.getSite(Site.defaultSiteId());
		SiteVo site = getDefSite();
		model.addAttribute("site", site);
		model.addAttribute("isIndex", true);
		return "modules/cms/front/themes/"+site.getTheme()+"/frontIndex";
	}
	
	private static SiteVo DEF_SITE=null;
	public SiteVo getDefSite(){
		if(DEF_SITE==null){
			DEF_SITE=siteService.findById("1");
		}
		return DEF_SITE;
	}
	
	/**
	 * 网站首页
	 */
	@RequestMapping(value = "index-{siteId}${urlSuffix}")
	public String index(@PathVariable String siteId, Model model) {
		if (siteId.equals("1")){
			return "redirect:"+frontPath;
		}
//		Site site = CmsApiUtils.getSite(siteId);
		SiteVo site = getDefSite();
		// 子站有独立页面，则显示独立页面
		if (StringUtils.isNotBlank(site.getCustomIndexView())){
			model.addAttribute("site", site);
			model.addAttribute("isIndex", true);
			return "modules/cms/front/themes/"+site.getTheme()+"/frontIndex"+site.getCustomIndexView();
		}
		// 否则显示子站第一个栏目
//		List<Category> mainNavList = CmsApiUtils.getMainNavList(siteId);
//		if (mainNavList.size() > 0){
//			String firstCategoryId = CmsApiUtils.getMainNavList(siteId).get(0).getId();
//			return "redirect:"+Global.getFrontPath()+"/list-"+firstCategoryId+Global.getUrlSuffix();
//		}else{
			model.addAttribute("site", site);
			return "modules/cms/front/themes/"+site.getTheme()+"/frontListCategory";
//		}
	}
	
	/**
	 * 内容列表
	 */
	@RequestMapping(value = "list-{categoryId}${urlSuffix}")
	public String list(@PathVariable String categoryId, @RequestParam(required=false, defaultValue="1") Integer pageNo,
			@RequestParam(required=false, defaultValue="15") Integer pageSize, Model model
			,HttpServletRequest request, HttpServletResponse response) {
//		Category category = categoryService.get(categoryId);
		CategoryVo category = categoryService.findById(categoryId);
		if (category==null){
//			Site site = CmsApiUtils.getSite(Site.defaultSiteId());
			SiteVo site = getDefSite();
			model.addAttribute("site", site);
			return "error/404";
		}
//		Site site = siteService.get(category.getSite().getId());
		SiteVo site = getDefSite();
		model.addAttribute("site", site);
		// 2：简介类栏目，栏目第一条内容
		if("2".equals(category.getShowModes()) && "article".equals(category.getModule())){
			// 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
			List<CategoryVo> categoryList = new ArrayList<CategoryVo>();
			if (category.getParent().getId().equals("1")){
				categoryList.add(category);
			}else{
				CategoryQo qo=new CategoryQo();
				qo.setParentId(category.getParent().getId());
				qo.setSiteId(category.getSite().getId());
//				categoryList = categoryService.findByParentId(category.getParent().getId(), category.getSite().getId());
				categoryList = categoryService.findList(qo);
				for(CategoryVo vo:categoryList){
					vo.setUrl(CategoryUtil.getUrlDynamic(vo, request.getContextPath(), frontPath, urlSuffix));
				}
			}
			model.addAttribute("category", category);
			model.addAttribute("categoryList", categoryList);
			// 获取文章内容
//			PageQo pageQo = new PageQo(1, 1, -1);
			PageQo pageQo = new PageQo();
			ArticleQo qo = new ArticleQo();
			qo.setCategoryId(categoryId);
//			page = articleService.findPage(page, article, false);
			Page<ArticleVo>pageVo = articleService.findPage(pageQo, qo);
			
			CmsApiUtils.addViewConfigAttribute(model, category);
			
			if (pageVo.getList().size()>0){
				ArticleVo articleVo = pageVo.getList().get(0);
//				article.setArticleData(articleDataService.get(article.getId()));
				articleService.updateHitsAddOne(articleVo.getId());
				model.addAttribute("article", articleVo);
				CmsApiUtils.addViewConfigAttribute(model, articleVo.getViewConfig());
				return "modules/cms/front/themes/"+site.getTheme()+"/"+getTpl(articleVo);
			}else{
				return "modules/cms/front/themes/"+site.getTheme()+"/"+ArticleVo.DEFAULT_TEMPLATE;
				
			}
		}else{
			CategoryQo qo=new CategoryQo();
			qo.setParentId(category.getId());
			qo.setSiteId(category.getSite().getId());
			List<CategoryVo> categoryList = new ArrayList<CategoryVo>();
			categoryList = categoryService.findList(qo);
			for(CategoryVo vo:categoryList){
				vo.setUrl(CategoryUtil.getUrlDynamic(vo, request.getContextPath(), frontPath, urlSuffix));
			}
//			List<Category> categoryList = categoryService.findByParentId(category.getId(), category.getSite().getId());
			// 展现方式为1 、无子栏目或公共模型，显示栏目内容列表
			if("1".equals(category.getShowModes())||categoryList.size()==0){
				// 有子栏目并展现方式为1，则获取第一个子栏目；无子栏目，则获取同级分类列表。
				if(categoryList.size()>0){
					category = categoryList.get(0);
				}else{
					// 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
					if (category.getParent().getId().equals("1")){
						categoryList.add(category);
					}else{
						qo.setParentId(category.getParent().getId());
						qo.setSiteId(category.getSite().getId());
//						categoryList = categoryService.findByParentId(category.getParent().getId(), category.getSite().getId());
						categoryList = categoryService.findList(qo);
						for(CategoryVo vo:categoryList){
							vo.setUrl(CategoryUtil.getUrlDynamic(vo, request.getContextPath(), frontPath, urlSuffix));
						}
					}
				}
				model.addAttribute("category", category);
				model.addAttribute("categoryList", categoryList);
				// 获取内容列表
				if ("article".equals(category.getModule())){
					PageQo pageQo = new PageQo(pageNo, pageSize);
					//System.out.println(page.getPageNo());
					ArticleQo qrticleQo=new ArticleQo();
					qrticleQo.setCategoryId(category.getId());
//					Page<ArticleVo>page = articleService.findPage(page, new Article(category), false);
					Page<ArticleVo>page = articleService.findPage(pageQo, qrticleQo);
					
					for(ArticleVo articleVo:page.getList()){
						articleVo.setUrl(ArticleUtil.getUrlDynamic(articleVo, request.getContextPath(), frontPath, urlSuffix));
					}
					
					model.addAttribute("page", page);
					// 如果第一个子栏目为简介类栏目，则获取该栏目第一篇文章
					if ("2".equals(category.getShowModes())){
						ArticleVo article = new ArticleVo();
						article.setCategory(category);
						if (page.getList().size()>0){
							article = page.getList().get(0);
							article.setArticleData(articleDataService.findById(article.getId()));
							articleService.updateHitsAddOne(article.getId());
						}
						model.addAttribute("article", article);
			            CmsApiUtils.addViewConfigAttribute(model, category);
			            CmsApiUtils.addViewConfigAttribute(model, article.getViewConfig());
						return "modules/cms/front/themes/"+site.getTheme()+"/"+getTpl(article);
					}
				}else if ("link".equals(category.getModule())){
//					Page<LinkVo> page = new Page<Link>(1, -1);
//					page = linkService.findPage(page, new Link(category), false);
					
					PageQo pageQo = new PageQo(pageNo, pageSize);
//					Page<LinkVo> page = linkService.findPage(page, new Link(category), false);
					Page<LinkVo> page = linkService.findPage(pageQo, new LinkQo());
					
					model.addAttribute("page", page);
				}
				String view = "/frontList";
				if (StringUtils.isNotBlank(category.getCustomListView())){
					view = "/"+category.getCustomListView();
				}
	            CmsApiUtils.addViewConfigAttribute(model, category);
//                site =siteService.get(category.getSite().getId());
	            site=this.getDefSite();
                //System.out.println("else 栏目第一条内容 _2 :"+category.getSite().getTheme()+","+site.getTheme());
//				return "modules/cms/front/themes/"+siteService.get(category.getSite().getId()).getTheme()+view;
	            return "modules/cms/front/themes/"+siteService.findById(category.getSite().getId()).getTheme()+view;
	            //return "modules/cms/front/themes/"+category.getSite().getTheme()+view;
			}
			// 有子栏目：显示子栏目列表
			else{
				model.addAttribute("category", category);
				model.addAttribute("categoryList", categoryList);
				String view = "/frontListCategory";
				if (StringUtils.isNotBlank(category.getCustomListView())){
					view = "/"+category.getCustomListView();
				}
	            CmsApiUtils.addViewConfigAttribute(model, category);
				return "modules/cms/front/themes/"+site.getTheme()+view;
			}
		}
	}
	private String getTpl(ArticleVo article) {
		if (StringUtils.isBlank(article.getCustomContentView())) {
			String view = null;
			CategoryVo c = article.getCategory();
			boolean goon = true;
			do {
				if (StringUtils.isNotBlank(c.getCustomContentView())) {
					view = c.getCustomContentView();
					goon = false;
				} else if (c.getParent() == null || c.getParent().isRoot()) {
					goon = false;
				} else {
					c = c.getParent();
				}
			} while (goon);
			return StringUtils.isBlank(view) ? ArticleVo.DEFAULT_TEMPLATE : view;
		} else {
			return article.getCustomContentView();
		}
	}
	/**
	 * 内容列表（通过url自定义视图）
	 */
	@RequestMapping(value = "listc-{categoryId}-{customView}${urlSuffix}")
	public String listCustom(@PathVariable String categoryId, @PathVariable String customView, @RequestParam(required=false, defaultValue="1") Integer pageNo,
			@RequestParam(required=false, defaultValue="15") Integer pageSize, Model model
			,HttpServletRequest request, HttpServletResponse response) {
		CategoryVo category = categoryService.findById(categoryId);
		if (category==null){
//			Site site = CmsApiUtils.getSite(Site.defaultSiteId());
			model.addAttribute("site", this.getDefSite());
			return "error/404";
		}
		SiteVo site = siteService.findById(category.getSite().getId());
		model.addAttribute("site", site);
		CategoryQo qo = new CategoryQo();
		qo.setParentId(category.getId());
		qo.setSiteId(category.getSite().getId());
		
//		List<CategoryVo> categoryList = categoryService.findByParentId(category.getId(), category.getSite().getId());
		List<CategoryVo> categoryList = categoryService.findList(qo);
		for(CategoryVo vo:categoryList){
			vo.setUrl(CategoryUtil.getUrlDynamic(vo, request.getContextPath(), frontPath, urlSuffix));
		}
		model.addAttribute("category", category);
		model.addAttribute("categoryList", categoryList);
        CmsApiUtils.addViewConfigAttribute(model, category);
		return "modules/cms/front/themes/"+site.getTheme()+"/frontListCategory"+customView;
	}

	/**
	 * 显示内容
	 */
	@RequestMapping(value = "view-{categoryId}-{contentId}${urlSuffix}")
	public String view(@PathVariable String categoryId, @PathVariable String contentId, Model model
			,HttpServletRequest request, HttpServletResponse response) {
//		CategoryVo category = categoryService.get(categoryId);
		CategoryVo category = categoryService.findById(categoryId);
		if (category==null){
//			Site site = CmsApiUtils.getSite(Site.defaultSiteId());
			model.addAttribute("site", this.getDefSite());
			return "error/404";
		}
		model.addAttribute("site", category.getSite());
		if ("article".equals(category.getModule())){
			// 如果没有子栏目，并父节点为跟节点的，栏目列表为当前栏目。
			List<CategoryVo> categoryList = new ArrayList<CategoryVo>();
			if (category.getParent().getId().equals("1")){
				categoryList.add(category);
			}else{
				
				CategoryQo qo = new CategoryQo();
				qo.setParentId(category.getParent().getId());
				qo.setSiteId(category.getSite().getId());
//				categoryList = categoryService.findByParentId(category.getParent().getId(), category.getSite().getId());
				categoryList = categoryService.findList(qo);
				for(CategoryVo vo:categoryList){
					vo.setUrl(CategoryUtil.getUrlDynamic(vo, request.getContextPath(), frontPath, urlSuffix));
				}
			}
			// 获取文章内容
			ArticleVo article = articleService.findById(contentId);
			if (article==null || !Constants.DEL_FLAG_NORMAL.equals(article.getDelFlag())){
				return "error/404";
			}
			// 文章阅读次数+1
			articleService.updateHitsAddOne(contentId);
			// 获取推荐文章列表
//			articleService.findByIdList(articleDataService.findById(article.getId()).getRelation());
//			List<Object[]> relationList = articleService.findByIds(articleDataService.findById(article.getId()).getRelation());
			List<Object[]> relationList = new ArrayList<Object[]>();
			// 将数据传递到视图
			CategoryVo c=categoryService.findById(article.getCategory().getId());
			c.setUrl(CategoryUtil.getUrlDynamic(c, request.getContextPath(), frontPath, urlSuffix));
			
			for(CategoryVo vo:categoryList){
				vo.setUrl(CategoryUtil.getUrlDynamic(vo, request.getContextPath(), frontPath, urlSuffix));
			}
			
			model.addAttribute("category", c);
			model.addAttribute("categoryList", categoryList);
			article.setArticleData(articleDataService.findById(article.getId()));
			model.addAttribute("article", article);
			model.addAttribute("relationList", relationList); 
            CmsApiUtils.addViewConfigAttribute(model, article.getCategory());
            CmsApiUtils.addViewConfigAttribute(model, article.getViewConfig());
//            Site site = siteService.get(category.getSite().getId());
            SiteVo site = this.getDefSite();
            model.addAttribute("site", site);
//			return "modules/cms/front/themes/"+category.getSite().getTheme()+"/"+getTpl(article);
            return "modules/cms/front/themes/"+site.getTheme()+"/"+getTpl(article);
		}
		return "error/404";
	}
	
	/**
	 * 内容评论
	 */
	@RequestMapping(value = "comment", method=RequestMethod.GET)
	public String comment(String theme, CommentVo comment, HttpServletRequest request, HttpServletResponse response, Model model) {
//		PageQo page = new PageQo(request, response);
		PageQo pageQo = super.getPage(request, response);
		CommentQo qo = new CommentQo();
//		qo.setCategory(comment.getCategory());
		qo.setCategoryId(comment.getCategory().getId());
		qo.setContentId(comment.getContentId());
		qo.setDelFlag(Constants.DEL_FLAG_NORMAL);
		Page page = commentService.findPage(pageQo, qo);
		model.addAttribute("page", page);
		model.addAttribute("comment", comment);
		return "modules/cms/front/themes/"+theme+"/frontComment";
	}
	
	/**
	 * 内容评论保存
	 */
	@ResponseBody
	@RequestMapping(value = "comment", method=RequestMethod.POST)
	public String commentSave(CommentVo comment, String validateCode,@RequestParam(required=false) String replyId, HttpServletRequest request) {
		if (StringUtils.isNotBlank(validateCode)){
			if (validate(request, validateCode)){
				if (StringUtils.isNotBlank(replyId)){
//					CommentVo replyComment = commentService.get(replyId);
					CommentVo replyComment = commentService.findById(replyId);
					if (replyComment != null){
						comment.setContent("<div class=\"reply\">"+replyComment.getName()+":<br/>"
								+replyComment.getContent()+"</div>"+comment.getContent());
					}
				}
				comment.setIp(request.getRemoteAddr());
				comment.setCreateDate(new Date());
				comment.setDelFlag(Constants.DEL_FLAG_AUDIT);
//				commentService.save(comment);
				commentService.add(comment);
				return "{result:1, message:'提交成功。'}";
			}else{
				return "{result:2, message:'验证码不正确。'}";
			}
		}else{
			return "{result:2, message:'验证码不能为空。'}";
		}
	}
	
	/**
	 * 站点地图
	 */
	@RequestMapping(value = "map-{siteId}${urlSuffix}")
	public String map(@PathVariable String siteId, Model model) {
//		Site site = CmsApiUtils.getSite(siteId!=null?siteId:Site.defaultSiteId());
		SiteVo site = getDefSite();
		model.addAttribute("site", site);
		return "modules/cms/front/themes/"+site.getTheme()+"/frontMap";
	}

	
}
