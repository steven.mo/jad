/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller.front;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.context.Constants;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.core.cms.qo.GuestbookQo;
import com.jad.core.cms.service.GuestbookService;
import com.jad.core.cms.vo.GuestbookVo;
import com.jad.web.mvc.BaseController;

/**
 * 留言板Controller
 * @author ThinkGem
 * @version 2013-3-15
 */
@Controller
@RequestMapping(value = "${frontPath}/guestbook")
public class FrontGuestbookController extends BaseController{
	
	@Autowired
	private GuestbookService guestbookService;

	/**
	 * 留言板
	 */
	@RequestMapping(value = "", method=RequestMethod.GET)
	public String guestbook(@RequestParam(required=false, defaultValue="1") Integer pageNo,
			@RequestParam(required=false, defaultValue="30") Integer pageSize, Model model) {
//		Site site = CmsUtils.getSite(Site.defaultSiteId());
//		model.addAttribute("site", site);
		
		PageQo page = new PageQo(pageNo, pageSize);
		GuestbookQo guestbook = new GuestbookQo();
		guestbook.setDelFlag(Constants.DEL_FLAG_NORMAL);
		Page<GuestbookVo>dataPage = guestbookService.findPage(page, guestbook);
		model.addAttribute("page", dataPage);
//		return "modules/cms/front/themes/"+site.getTheme()+"/frontGuestbook";
		return "modules/cms/front/themes/basic/frontGuestbook";
	}
	
	/**
	 * 留言板-保存留言信息
	 */
	@RequestMapping(value = "", method=RequestMethod.POST)
	public String guestbookSave(GuestbookVo guestbook, String validateCode, HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) {
//		if (StringUtils.isNotBlank(validateCode)){
//			if (ValidateCodeServlet.validate(request, validateCode)){
				guestbook.setIp(request.getRemoteAddr());
				guestbook.setCreateDate(new Date());
				guestbook.setDelFlag(Constants.DEL_FLAG_AUDIT);
				guestbookService.add(guestbook);
				addMessage(redirectAttributes, "提交成功，谢谢！");
//			}else{
//				addMessage(redirectAttributes, "验证码不正确。");
//			}
//		}else{
//			addMessage(redirectAttributes, "验证码不能为空。");
//		}
//		return "redirect:"+Global.getFrontPath()+"/guestbook";
				return "redirect:"+frontPath+"/guestbook";
	}
	
}
