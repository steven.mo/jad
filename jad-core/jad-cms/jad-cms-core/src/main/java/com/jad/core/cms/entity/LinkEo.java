/**
\ * Copyright &copy; 2012-2013 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.BaseEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;

/**
 * 链接Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
@Entity
@Table(name = "cms_link")
public class LinkEo extends BaseEo<String> {
	
	private static final long serialVersionUID = 1L;
	private CategoryEo category;// 分类编号
	private String title;	// 链接名称
	private String color;	// 标题颜色（red：红色；green：绿色；blue：蓝色；yellow：黄色；orange：橙色）
	private String image;	// 链接图片
	private String href;	// 链接地址
	private Integer weight;	// 权重，越大越靠前
	private Date weightDate;// 权重期限，超过期限，将weight设置为0

	public LinkEo() {
		super();
		this.weight = 0;
	}
	
	public LinkEo(String id){
		this();
		this.id = id;
	}

//	public LinkEo(CategoryEo category){
//		this();
//		this.category = category;
//	}

	@ManyToOne
	@RelateColumn(name="category_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	@NotNull
	public CategoryEo getCategory() {
		return category;
	}

	public void setCategory(CategoryEo category) {
		this.category = category;
	}

	@Column(name="title")
	@Length(min=1, max=255)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="color")
	@Length(min=0, max=50)
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	@Column(name="image")
	@Length(min=0, max=255)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Column(name="href")
	@Length(min=0, max=255)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	@Column(name="weight")
	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	@Column(name="weight_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getWeightDate() {
		return weightDate;
	}

	public void setWeightDate(Date weightDate) {
		this.weightDate = weightDate;
	}

	
}