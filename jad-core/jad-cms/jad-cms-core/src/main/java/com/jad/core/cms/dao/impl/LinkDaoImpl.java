package com.jad.core.cms.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.jad.core.cms.dao.LinkDao;
import com.jad.core.cms.entity.LinkEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("linkDao")
public class LinkDaoImpl extends AbstractJadEntityDao<LinkEo,String>implements LinkDao{

//	@Override
//	public List<LinkEo> findByIdIn(String[] ids) {
//		// TODO Auto-generated method stub
//		
//		Assert.notEmpty(ids);
//		
//		List<Object>params=new ArrayList();
//		
//		StringBuffer sb=new StringBuffer();
//		sb.append(SqlHelper.getSelectSql(LinkEo.class, "a", params));
//		sb.append(" where ");
//		sb.append(SqlHelper.getValsInSql(Arrays.asList(ids), params));
//		
//		
//		return super.findBySql(sb.toString(), params, LinkEo.class);
//	}

	@Override
	public int updateExpiredWeight() {
		String sql="update cms_link SET weight = 0 WHERE weight > 0 "
				+ "AND weight_date < CURDATE()  ";
		
		List<Object>params=new ArrayList();
		
		return super.executeSql(sql, params);
	}


}
