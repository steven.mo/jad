/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.QueryObject;
import com.jad.core.cms.entity.ArticleDataEo;
import com.jad.core.cms.service.ArticleDataService;
import com.jad.core.cms.vo.ArticleDataVo;

/**
 * 站点Service
 * @author ThinkGem
 * @version 2013-01-15
 */
@Service("articleDataService")
@Transactional(readOnly = true)
public class ArticleDataServiceImpl extends AbstractServiceImpl<ArticleDataEo,String,ArticleDataVo> implements ArticleDataService {

}
