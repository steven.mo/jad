/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.TreeEo;
import com.jad.core.sys.entity.OfficeEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;

/**
 * 栏目Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
@Entity
@Table(name = "cms_category")
public class CategoryEo extends TreeEo<CategoryEo,String> {

    public static final String DEFAULT_TEMPLATE = "frontList";

	private static final long serialVersionUID = 1L;
	
	private CategoryEo parent;	// 父级编号
	
	private SiteEo site;		// 归属站点
	private String module; 	// 栏目模型（article：文章；picture：图片；download：下载；link：链接；special：专题）
	private String image; 	// 栏目图片
	private String href; 	// 链接
	private String target; 	// 目标（ _blank、_self、_parent、_top）
	private String description; 	// 描述，填写有助于搜索引擎优化
	private String keywords; 	// 关键字，填写有助于搜索引擎优化
	private String inMenu; 		// 是否在导航中显示（1：显示；0：不显示）
	private String inList; 		// 是否在分类页中显示列表（1：显示；0：不显示）
	private String showModes; 	// 展现方式（0:有子栏目显示栏目列表，无子栏目显示内容列表;1：首栏目内容列表；2：栏目第一条内容）
	private String allowComment;// 是否允许评论
	private String isAudit;	// 是否需要审核
	private String customListView;		// 自定义列表视图
	private String customContentView;	// 自定义内容视图
    private String viewConfig;	// 视图参数
    
    private OfficeEo office;//归属部门
    

	public CategoryEo(){
		super();
//		this.module = "";
//		this.sort = 30;
//		this.inMenu = Constants.HIDE;
//		this.inList = Constants.SHOW;
//		this.showModes = "0";
//		this.allowComment = Constants.NO;
//		this.delFlag = Constants.DEL_FLAG_NORMAL;
//		this.isAudit = Constants.NO;
	}

	public CategoryEo(String id){
		this();
		this.id = id;
	}
	
	
	@ManyToOne
	@RelateColumn(name="site_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	@OrderBy("site_id")
	public SiteEo getSite() {
		return site;
	}


	public void setSite(SiteEo site) {
		this.site = site;
	}
	

//	@JsonBackReference
//	@NotNull
	@ManyToOne
	@RelateColumn(name="parent_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public CategoryEo getParent() {
		return parent;
	}

	public void setParent(CategoryEo parent) {
		this.parent = parent;
	}
	
	@Column(name="module")
	@Length(min=0, max=20)
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}


	@Column(name="image")
	@Length(min=0, max=255)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Column(name="href")
	@Length(min=0, max=255)
	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	@Column(name="target")
	@Length(min=0, max=20)
	public String getTarget() {
		return target;
	}


	public void setTarget(String target) {
		this.target = target;
	}

	@Column(name="description")
	@Length(min=0, max=255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name="keywords")
	@Length(min=0, max=255)
	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	@Column(name="in_menu")
	@Length(min=1, max=1)
	public String getInMenu() {
		return inMenu;
	}

	public void setInMenu(String inMenu) {
		this.inMenu = inMenu;
	}

	@Column(name="in_list")
	@Length(min=1, max=1)
	public String getInList() {
		return inList;
	}

	public void setInList(String inList) {
		this.inList = inList;
	}

	@Column(name="show_modes")
	@Length(min=1, max=1)
	public String getShowModes() {
		return showModes;
	}

	public void setShowModes(String showModes) {
		this.showModes = showModes;
	}
	
	@Column(name="allow_comment")
	@Length(min=1, max=1)
	public String getAllowComment() {
		return allowComment;
	}

	public void setAllowComment(String allowComment) {
		this.allowComment = allowComment;
	}

	@Column(name="is_audit")
	@Length(min=1, max=1)
	public String getIsAudit() {
		return isAudit;
	}

	public void setIsAudit(String isAudit) {
		this.isAudit = isAudit;
	}

	@Column(name="custom_list_view")
	public String getCustomListView() {
		return customListView;
	}

	public void setCustomListView(String customListView) {
		this.customListView = customListView;
	}

	@Column(name="custom_content_view")
	public String getCustomContentView() {
		return customContentView;
	}

	public void setCustomContentView(String customContentView) {
		this.customContentView = customContentView;
	}

	@Column(name="view_config")
    public String getViewConfig() {
        return viewConfig;
    }

    public void setViewConfig(String viewConfig) {
        this.viewConfig = viewConfig;
    }

    
	@ManyToOne
	@RelateColumn(name="office_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public OfficeEo getOffice() {
		return office;
	}

	public void setOffice(OfficeEo office) {
		this.office = office;
	}
	

    
}