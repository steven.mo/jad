/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.JadTreeServiceImpl;
import com.jad.commons.util.QoUtil;
import com.jad.commons.utils.Validate;
import com.jad.core.cms.dao.CategoryDao;
import com.jad.core.cms.entity.CategoryEo;
import com.jad.core.cms.qo.CategoryQo;
import com.jad.core.cms.service.CategoryService;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.dao.utils.EntityUtils;

/**
 * 栏目Service
 * @author ThinkGem
 * @version 2013-5-31
 */
@Service("categoryService")
@Transactional(readOnly = true)
public class CategoryServiceImpl extends JadTreeServiceImpl<CategoryEo,String, CategoryVo> 
	implements CategoryService{

//	public static final String CACHE_CATEGORY_LIST = "categoryList";
	
//	@Autowired
//	private SiteService siteService;
	
//	@Autowired
//	private CategoryCao categoryCao;
	
	@Autowired
	private CategoryDao categoryDao;
	
//	public List<CategoryVo> findByUser(boolean isCurrentSite, String module){
//		
////		List<CategoryVo> list = (List<CategoryVo>)UserUtils.getCache(CACHE_CATEGORY_LIST);
//		List<CategoryVo> list =null;
//		if (list == null){
////			UserVo user = UserUtils.getUser();
//			CategoryQo category = new CategoryQo();
////			category.setOffice(new OfficeVo());
////			category.getSqlMap().put("dsf", DataFilters.dataScopeFilter(user, "o", "u"));
////			category.setSiteId(new SiteVo());
////			category.setParentId(new CategoryVo());
//			list = EntityUtils.copyToVoList(categoryDao.findByQo(category), CategoryVo.class);
//			// 将没有父节点的节点，找到父节点
//			Set<String> parentIdSet = Sets.newHashSet();
//			for (CategoryVo e : list){
//				if (e.getParent()!=null && StringUtils.isNotBlank(e.getParent().getId())){
//					boolean isExistParent = false;
//					for (CategoryVo e2 : list){
//						if (e.getParent().getId().equals(e2.getId())){
//							isExistParent = true;
//							break;
//						}
//					}
//					if (!isExistParent){
//						parentIdSet.add(e.getParent().getId());
//					}
//				}
//			}
//			if (parentIdSet.size() > 0){
//				//FIXME 暂且注释，用于测试
////				dc = dao.createDetachedCriteria();
////				dc.add(Restrictions.in("id", parentIdSet));
////				dc.add(Restrictions.eq("delFlag", Category.DEL_FLAG_NORMAL));
////				dc.addOrder(Order.asc("site.id")).addOrder(Order.asc("sort"));
////				list.addAll(0, dao.find(dc));
//			}
////			UserUtils.putCache(CACHE_CATEGORY_LIST, list);
//		}
//		
//		if (isCurrentSite){
//			List<CategoryVo> categoryList = Lists.newArrayList(); 
//			for (CategoryVo e : list){
//				if (CategoryUtil.isRoot(e.getId()) || (e.getSite()!=null && e.getSite().getId() !=null 
//						&& e.getSite().getId().equals(siteService.getCurrentSiteId()))){
//					if (StringUtils.isNotEmpty(module)){
//						if (module.equals(e.getModule()) || "".equals(e.getModule())){
//							categoryList.add(e);
//						}
//					}else{
//						categoryList.add(e);
//					}
//				}
//			}
//			return categoryList;
//		}
//		return list;
//	}

//	public List<CategoryVo> findByParentId(String parentId, String siteId){
//		
//		Validate.allNotBlank(parentId,siteId);
//		
//		CategoryQo qo = QoUtil.newNormalDataQo(CategoryQo.class);
//		qo.setParentId(parentId);
//		qo.setSiteId(siteId);
//		
//		return EntityUtils.copyToVoList(categoryDao.findByQo(qo), CategoryVo.class);
//	}
	
//	public Page<CategoryVo> find(Page<CategoryVo> page, CategoryQo category) {
////		DetachedCriteria dc = dao.createDetachedCriteria();
////		if (category.getSite()!=null && StringUtils.isNotBlank(category.getSite().getId())){
////			dc.createAlias("site", "site");
////			dc.add(Restrictions.eq("site.id", category.getSite().getId()));
////		}
////		if (category.getParent()!=null && StringUtils.isNotBlank(category.getParent().getId())){
////			dc.createAlias("parent", "parent");
////			dc.add(Restrictions.eq("parent.id", category.getParent().getId()));
////		}
////		if (StringUtils.isNotBlank(category.getInMenu()) && Category.SHOW.equals(category.getInMenu())){
////			dc.add(Restrictions.eq("inMenu", category.getInMenu()));
////		}
////		dc.add(Restrictions.eq(Category.FIELD_DEL_FLAG, Category.DEL_FLAG_NORMAL));
////		dc.addOrder(Order.asc("site.id")).addOrder(Order.asc("sort"));
////		return dao.find(page, dc);
////		page.setSpringPage(dao.findByParentId(category.getParent().getId(), page.getSpringPage()));
////		return page;
//		
////		category.setPage(page);
//		category.setInMenu(Constants.SHOW);
////		page.setList(categoryDao.findModule(category));
//		
//		page.setList(EntityUtils.copyToVoList(categoryDao.findModule(category), CategoryVo.class));
//		
//		return page;
//	}
	
//	@Transactional(readOnly = false)
//	public void save(CategoryVo category) {
//		category.setSite(new SiteVo(siteService.getCurrentSiteId()));
//		if (StringUtils.isNotBlank(category.getViewConfig())){
//            category.setViewConfig(StringEscapeUtils.unescapeHtml4(category.getViewConfig()));
//        }
//		if(StringUtils.isNotBlank(category.getId())){
//			super.update(category);
//		}else{
//			super.add(category);
//		}
////		super.save(category);
//		
////		UserUtils.removeCache(CACHE_CATEGORY_LIST);
////		CmsUtils.removeCache("mainNavList_"+category.getSite().getId());
////		categoryCao.removeMainNavList(category.getSite().getId());
//		
//	}
	
//	@Transactional(readOnly = false)
//	public void delete(CategoryVo category) {
//		super.delete(category);
////		UserUtils.removeCache(CACHE_CATEGORY_LIST);
////		CmsUtils.removeCache("mainNavList_"+category.getSite().getId());
////		categoryCao.removeMainNavList(category.getSite().getId());
//	}
	
//	/**
//	 * 通过编号获取栏目列表
//	 */
//	public List<CategoryVo> findByIds(String ids) {
//		List<CategoryVo> list = Lists.newArrayList();
//		String[] idss = StringUtils.split(ids,",");
//		if (idss.length>0){
////			List<Category> l = dao.findByIdIn(idss);
////			for (String id : idss){
////				for (Category e : l){
////					if (e.getId().equals(id)){
////						list.add(e);
////						break;
////					}
////				}
////			}
//			for(String id : idss){
//				CategoryVo e = EntityUtils.copyToVo(categoryDao.findById(id), CategoryVo.class);
//				if(null != e){
//					//System.out.println("e.id:"+e.getId()+",e.name:"+e.getName());
//					list.add(e);
//				}
//				//list.add(dao.get(id));
//				
//			}
//		}
//		return list;
//	}
	
}
