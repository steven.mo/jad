/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.utils;

import java.util.List;

import com.jad.bpm.dto.ActActivity;
import com.jad.bpm.dto.ActProcessDefinition;
import com.jad.commons.utils.ObjectUtils;

/**
 * 流程定义缓存
 * @author ThinkGem
 * @version 2013-12-05
 */
public class ProcessDefCache {

//	private static final String ACT_CACHE = "actCache";
//	private static final String ACT_CACHE_PD_ID_ = "pd_id_";
	
	/**
	 * 获得流程定义对象
	 * @param procDefId
	 * @return
	 */
	public static ActProcessDefinition get(String procDefId) {
//		ProcessDefinitionCao cao = SpringContextHolder.getBean(ProcessDefinitionCao.class);
//		ProcessDefinitionCao cao = SpringContextHolder.getBean("processDefinitionCao");
//		ActProcessDefinition actPd =cao.getProcessDefinition(procDefId);
//		if (actPd == null) {
////			RepositoryService repositoryService = SpringContextHolder.getBean(RepositoryService.class);
//			RepositoryService repositoryService = SpringContextHolder.getBean("repositoryService");
//			ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionId(procDefId).singleResult();
//			if (pd != null) {
//				actPd=ActUtils.toActProcessDefinition(pd);
//				cao.putProcessDefinition(procDefId, actPd);
//			}
//		}
//		return actPd;
		return null;
	}

	/**
	 * 获得流程定义的所有活动节点
	 * @param procDefId
	 * @return
	 */
	public static List<ActActivity> getActivitys(String procDefId) {
		ActProcessDefinition pd = get(procDefId);
		if (pd != null) {
//			return ((ProcessDefinitionEntity) pd).getActivities();
			return pd.getActivityList();
		}
		return null;
	}
	
	/**
	 * 获得流程定义活动节点
	 * @param procDefId
	 * @param activityId
	 * @return
	 */
	public static ActActivity getActivity(String procDefId, String activityId) {
		ActProcessDefinition pd = get(procDefId);
		if (pd != null) {
			List<ActActivity> list = getActivitys(procDefId);
			if (list != null){
				for (ActActivity activityImpl : list) {
					if (activityId.equals(activityImpl.getId())){
						return activityImpl;
					}
				}
			}
		}
		return null;
	}

	/**
	 * 获取流程定义活动节点名称
	 * @param procDefId
	 * @param activityId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String getActivityName(String procDefId, String activityId) {
		ActActivity activity = getActivity(procDefId, activityId);
		if (activity != null) {
			return ObjectUtils.toString(activity.getProperty("name"));
		}
		return null;
	}

}
