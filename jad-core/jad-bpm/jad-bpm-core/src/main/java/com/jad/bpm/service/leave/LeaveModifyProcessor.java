package com.jad.bpm.service.leave;

import org.activiti.engine.delegate.DelegateTask;

/**
 * 调整请假内容处理器
 * @author liuj
 */
public interface LeaveModifyProcessor   {
	
	public void notify(DelegateTask delegateTask) ;

}
