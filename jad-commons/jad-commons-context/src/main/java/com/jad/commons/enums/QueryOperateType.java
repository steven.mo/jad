/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jad.commons.enums;

/**
 * <p>
 * 生成ID类型枚举类
 * </p>
 * 
 * @author hubin
 * @Date 2015-11-10
 */
public enum QueryOperateType {
	eq("eq","Eq", "=","等同于 field=value"), 
	gt("gt","BeginGt",">", "等同于 field>value"),
	ge("ge","Begin", ">=","等同于 field>=value"),
	lt("lt","EndLt", "<","等同于field<value表达式"),
	le("le","End", "<=","等同于field<=value表达式"),
	
	like("like","Like","like", "等同于field like '%value表达式%'"),
	likeLeft("likeLeft","LikeLeft","like", "等同于field like '%value表达式'"),
	likeRight("likeRight","LikeRight","like", "等同于field like 'value表达式%'"),
	
	notLike("notLike","NotLike", "not like","等同于field not like '%value表达式%'"),
	notLikeLeft("notLikeLeft","NotLikeLeft", "not like","等同于field not like '%value表达式'"),
	notLikeRight("notLikeRight","NotLikeRight","not like", "等同于field not like 'value表达式%'"),
	
	isNull("isNull","IsNull", "is null","等同于field is null"),
	isNotNull("isNotNull","IsNotNull","is not null", "等同于field is not null"),
	
	in("in","In","in", "等同于field in (values)"),
	notIn("notIn","NotIn", "not in","等同于field not in (values)"),
	
	between("between","Between","between", "等同于field between (value1,value2)"),
	;
	
	public static boolean isSimpleSingleOperate(String type){
		return eq.getType().equals(type) || gt.getType().equals(type) 
				|| ge.getType().equals(type) || lt.getType().equals(type) 
				|| le.getType().equals(type) ;
	}
	
	private final String type;
	
	private final String suffix;
	
	private final String desc;
	
	private final String expression;

	QueryOperateType(final String type,final String suffix ,final String expression, final String desc) {
		this.type = type;
		this.expression = expression;
		this.suffix = suffix;
		this.desc = desc;
	}

//	/**
//	 * <p>
//	 * 主键策略 （默认 ID_WORKER）
//	 * </p>
//	 * 
//	 * @param idType
//	 *            ID 策略类型
//	 * @return
//	 */
//	public static WhereOperateType getIdType(int idType) {
//		WhereOperateType[] its = WhereOperateType.values();
//		for (WhereOperateType it : its) {
//			if (it.getKey() == idType) {
//				return it;
//			}
//		}
//		return EQ;
//	}


	public String getDesc() {
		return this.desc;
	}

	public String getType() {
		return type;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getExpression() {
		return expression;
	}

}
