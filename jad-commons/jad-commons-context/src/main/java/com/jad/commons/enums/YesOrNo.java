package com.jad.commons.enums;

public enum YesOrNo {

	YES("1", "是"), 
	NO("0", "否");
	
	private String val;

	private String desc;
	
	

	YesOrNo(String val, String desc) {
		this.val = val;
		this.desc = desc;
	}


	public String getVal() {
		return val;
	}


	public void setVal(String val) {
		this.val = val;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}

	
	
}
