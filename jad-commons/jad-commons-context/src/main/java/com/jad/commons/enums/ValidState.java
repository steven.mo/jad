package com.jad.commons.enums;

public enum ValidState {



	VALID("1", "有效"), 
	INVALID("0", "无效");
	
	private String val;

	private String desc;

	ValidState(String val, String desc) {
		this.val = val;
		this.desc = desc;
	}


	public String getVal() {
		return val;
	}


	public void setVal(String val) {
		this.val = val;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}

	
	

}
