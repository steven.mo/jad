package com.jad.commons.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.commons.enums.CurdType;

/**
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD})
public @interface ApiCurdType {
	
	
	/**
	 * @return
	 */
	CurdType value() ;
	

}

