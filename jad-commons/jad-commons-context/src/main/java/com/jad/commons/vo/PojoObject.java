package com.jad.commons.vo;

import java.io.Serializable;

/**
 * 所有POJO类的父类，包括 VO,EO,QO 等
 * 
 * @author Administrator
 *
 */
public interface PojoObject extends Serializable {


}
