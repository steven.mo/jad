package com.jad.commons.http;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class HttpResp  implements Serializable{
	
	
	
	/**
	 * 响应头
	 */
	private Map<String,String>headMap;
	
	/**
	 * cookie
	 */
	private List<String>cookieList;
	
	/**
	 * 响应信息
	 */
	private String respMsg;
	
	
	/**
	 * 响应状态码
	 */
	private String respStatusCode;
	
	
//	
	
	
	public static void main(String[] args)   {
		
		
	}
	
	
	public String getRespStatusCode() {
		return respStatusCode;
	}


	public void setRespStatusCode(String respStatusCode) {
		this.respStatusCode = respStatusCode;
	}


	@Override
	public String toString() {
		return new Gson().toJson(this);
	}


	public String getRespMsg() {
		return respMsg;
	}


	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}


	public Map<String, String> getHeadMap() {
		return headMap;
	}
	public void setHeadMap(Map<String, String> headMap) {
		this.headMap = headMap;
	}
	public List<String> getCookieList() {
		return cookieList;
	}
	public void setCookieList(List<String> cookieList) {
		this.cookieList = cookieList;
	}




	
	
	
}
