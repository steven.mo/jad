/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.util;

import java.util.ArrayList;
import java.util.List;

import com.jad.commons.context.SpringContextHolder;
import com.jad.commons.json.JsonMapper;
import com.jad.commons.utils.StringUtils;
import com.jad.core.sys.service.DictService;
import com.jad.core.sys.vo.DictVo;

/**
 * 字典工具类
 * @author ThinkGem
 * @version 2013-5-29
 */
public class DictHelper {

	private static DictService dictService ;
	
	public static void init(){
		dictService = SpringContextHolder.getBean("dictService");
	}
	
	public static String getDictLabel(String value, String type, String defaultLabel){
		if (StringUtils.isBlank(type) || StringUtils.isBlank(value)){
			return defaultLabel;
		}
		return dictService.findDictLabel(value, type, defaultLabel);
		
	}
	
	public static String getDictLabels(String values, String type, String defaultLabel){
		if (StringUtils.isBlank(type) || StringUtils.isBlank(values)){
			return defaultLabel;
		}
		List<String> valueList =  new ArrayList();
		for (String value : StringUtils.split(values, ",")){
			valueList.add(getDictLabel(value, type, defaultLabel));
		}
		return StringUtils.join(valueList, ",");
	}

	public static String getDictValue(String label, String type, String defaultValue){
		if (StringUtils.isBlank(type) || StringUtils.isBlank(label)){
			return defaultValue;
		}
		return dictService.getDictValue(label, type, defaultValue);
	}
	
	public static List<DictVo> getDictList(String type){
		
		if(StringUtils.isBlank(type)){
			return new ArrayList<DictVo>();
		}
		return dictService.findByType(type);
	}
	
	/**
	 * 返回字典列表（JSON）
	 * @param type
	 * @return
	 */
	public static String getDictListJson(String type){
		return JsonMapper.toJsonString(getDictList(type));
	}
	
}
