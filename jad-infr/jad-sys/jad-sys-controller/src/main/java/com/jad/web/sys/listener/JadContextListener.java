package com.jad.web.sys.listener;

import javax.servlet.ServletContext;

import org.springframework.core.SpringProperties;
import org.springframework.web.context.WebApplicationContext;

import com.jad.commons.web.listener.WebContextListener;
import com.jad.web.sys.util.DictHelper;
import com.jad.web.sys.util.UserHelper;

public class JadContextListener extends WebContextListener{
 
 @SuppressWarnings("unused")
 public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
  WebApplicationContext wc=super.initWebApplicationContext(servletContext);

  DictHelper.init();
  UserHelper.init();

  return wc;
 }
 
}
