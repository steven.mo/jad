/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.service;

import java.util.List;

import com.jad.commons.service.CurdService;
import com.jad.core.sys.vo.DictVo;

/**
 * 字典Service
 * @author ThinkGem
 * @version 2014-05-16
 */
public interface DictService extends CurdService<DictVo,String>{
	
	/**
	 * 查询字段类型列表
	 * @return
	 */
	public List<String> findTypeList();
	
	public String findDictLabel(String value, String type, String defaultLabel);
	
	public String getDictValue(String label, String type, String defaultValue);
	
	public List<DictVo> findByType(String type);


}
