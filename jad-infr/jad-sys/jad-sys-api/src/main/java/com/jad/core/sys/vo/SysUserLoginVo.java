package com.jad.core.sys.vo;

import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.ValueObject;

import java.util.Date;

public class SysUserLoginVo implements ValueObject {

	private static final long serialVersionUID = 1L;

	// @CURD(label="${attr.paramValMap.label}",required=true)

	private Integer id; // id

	private String userName; // user_name

	private Date loginTime; // login_time

	private String token; // token

	private Date actTime;

	public Date getActTime() {
		return actTime;
	}

	public void setActTime(Date actTime) {
		this.actTime = actTime;
	}

	public SysUserLoginVo() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
