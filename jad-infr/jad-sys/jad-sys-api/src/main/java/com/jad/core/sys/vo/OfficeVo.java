package com.jad.core.sys.vo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.TreeVo;

public class OfficeVo extends TreeVo<OfficeVo,String> {

	private static final long serialVersionUID = 1L;
	
	private OfficeVo parent;
	
	@CURD(label="所属区域")
	private AreaVo area;		// 归属区域
	
	@CURD(label="机构编码")
	private String code; 	// 机构编码
	
	@CURD(label="机构类型")
	private String type; 	// 机构类型（1：公司；2：部门；3：小组）
	
	@CURD(label="机构等级")
	private String grade; 	// 机构等级（1：一级；2：二级；3：三级；4：四级）
	
	@CURD(label="联系地址")
	private String address; // 联系地址
	
	@CURD(label="邮政编码")
	private String zipCode; // 邮政编码
	
	@CURD(label="负责人")
	private String master; 	// 负责人
	
	@CURD(label="电话")
	private String phone; 	// 电话
	
	@CURD(label="传真")
	private String fax; 	// 传真
	
	@CURD(label="邮箱")
	private String email; 	// 邮箱
	
	@CURD(label="是否可用")
	private String useable;//是否可用
	
	private UserVo primaryPerson;//主负责人
	private UserVo deputyPerson;//副负责人
	
	public OfficeVo(){
		super();
	}

	public OfficeVo(String id){
		super(id);
	}
	

	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}

	@JsonIgnore
	public UserVo getPrimaryPerson() {
		return primaryPerson;
	}

	public void setPrimaryPerson(UserVo primaryPerson) {
		this.primaryPerson = primaryPerson;
	}

	@JsonIgnore
	public UserVo getDeputyPerson() {
		return deputyPerson;
	}

	public void setDeputyPerson(UserVo deputyPerson) {
		this.deputyPerson = deputyPerson;
	}

	@JsonBackReference
	public OfficeVo getParent() {
		return parent;
	}

	public void setParent(OfficeVo parent) {
		this.parent = parent;
	}

//	@JsonIgnore
	public AreaVo getArea() {
		return area;
	}

	public void setArea(AreaVo area) {
		this.area = area;
	}
	
	@Length(min=1, max=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Length(min=1, max=1)
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Length(min=0, max=255)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Length(min=0, max=100)
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Length(min=0, max=100)
	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	@Length(min=0, max=200)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Length(min=0, max=200)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Length(min=0, max=200)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Length(min=0, max=100)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


}
