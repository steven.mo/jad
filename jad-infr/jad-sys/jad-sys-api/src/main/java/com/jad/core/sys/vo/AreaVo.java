package com.jad.core.sys.vo;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.TreeVo;

/**
 * 区域
 *
 */
public class AreaVo extends TreeVo<AreaVo,String> {

	private static final long serialVersionUID = 1L;
	
	
	private AreaVo parent;
	
	@CURD(label = "区域编码",required=true)
	private String code; 	// 区域编码
	
	@CURD(label = "区域类型",required=true)
	private String type; 	// 区域类型（1：国家；2：省份、直辖市；3：地市；4：区县）
	
	public AreaVo(){
		super();
	}

	public AreaVo(String id){
		super(id);
	}
	
	@JsonBackReference
	public AreaVo getParent() {
		return parent;
	}

	public void setParent(AreaVo parent) {
		this.parent = parent;
	}

	@Length(min=1, max=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Length(min=0, max=100)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
