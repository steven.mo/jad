package com.jad.core.sys.qo;

import com.jad.commons.qo.BaseQo;

import java.util.Date;

import com.jad.commons.vo.QueryObject;

public class SysUserLoginQo implements QueryObject {

	private static final long serialVersionUID = 1L;

	// @CURD(label="${attr.paramValMap.label}")

	private Long id; // id

	private String userName; // user_name

	private Date loginTime; // login_time

	private String token; // token

	private Date actTime;

	public Date getActTime() {
		return actTime;
	}

	public void setActTime(Date actTime) {
		this.actTime = actTime;
	}

	public SysUserLoginQo() {
		super();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
