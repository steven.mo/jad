/**
 */
package com.jad.core.sys.service;


import com.jad.commons.service.CurdService;
import com.jad.core.sys.vo.SysUserLoginVo;

/**
 * 用户登录Service
 */
public interface SysUserLoginService extends CurdService<SysUserLoginVo,Long>{
	
	
	String refreshToken(String userName);
	
	Boolean checkToken(String token);
}



