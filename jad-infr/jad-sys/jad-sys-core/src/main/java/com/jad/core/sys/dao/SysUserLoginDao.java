/**
 */
package com.jad.core.sys.dao;


import com.jad.core.sys.entity.SysUserLoginEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 用户登录Dao
 */
@JadDao
public interface SysUserLoginDao extends JadEntityDao<SysUserLoginEo,Long> {
	
	Boolean checkToken(String token);
	
}
