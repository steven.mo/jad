/**
 */
package com.jad.core.sys.service.impl;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.utils.IdGen;
import com.jad.core.sys.dao.SysUserLoginDao;
import com.jad.core.sys.entity.SysUserLoginEo;
import com.jad.core.sys.qo.SysUserLoginQo;
import com.jad.core.sys.service.SysUserLoginService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.vo.SysUserLoginVo;


/**
 * 用户登录Service
 */
@Service("sysUserLoginService")
@Transactional(readOnly = true)
public class SysUserLoginServiceImpl extends AbstractServiceImpl<SysUserLoginEo, Long,SysUserLoginVo> implements SysUserLoginService{

	@Autowired
	private UserService userService;
	
	
	@Autowired
	private SysUserLoginDao sysUserLoginDao;
	
	@Transactional(readOnly = false)
	public String refreshToken(String userName){
		String token = IdGen.uuid();
		SysUserLoginVo vo = findByUserName(userName);
		if(vo==null){
			vo = new SysUserLoginVo();
			vo.setLoginTime(new Date());
			vo.setToken(token);
			vo.setUserName(userName);
			vo.setActTime(new Date());
			add(vo);
		}else{
			vo.setLoginTime(new Date());
			vo.setActTime(new Date());
			vo.setToken(token);
			update(vo);
		}
		return token;
	}
	
	@Transactional(readOnly = false)
	public Boolean checkToken(String token){
		return sysUserLoginDao.checkToken(token);
	}
	
	private SysUserLoginVo findByUserName(String userName) {
		SysUserLoginQo qo = new SysUserLoginQo();
		qo.setUserName(userName);
		return super.findOne(qo);
	}
 


}
