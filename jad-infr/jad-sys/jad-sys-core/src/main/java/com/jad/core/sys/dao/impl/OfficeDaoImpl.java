package com.jad.core.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.jad.commons.context.Constants;
import com.jad.commons.dao.AbstractTreeDao;
import com.jad.core.sys.dao.OfficeDao;
import com.jad.core.sys.entity.OfficeEo;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;

@JadDao("officeDao")
public class OfficeDaoImpl extends AbstractTreeDao<OfficeEo,String>implements OfficeDao{
	
	/**
	 * 跟据角色id查找部门
	 * @param roleId
	 * @return
	 */
	public List<OfficeEo> findByRoleId(String roleId) {
		EoMetaInfo<OfficeEo> ei=EntityUtils.getEoInfo(OfficeEo.class);
		List params= new ArrayList();
		String alias="a";
		StringBuffer sb=new StringBuffer();
		
		sb.append("select ");
		
		sb.append(SqlHelper.getSelectColumn(ei,alias));
		
		sb.append(" from ");
		
		sb.append(SqlHelper.getSelectFrom(ei,alias,params));
		sb.append(" JOIN sys_role_office rm ON rm.office_id = a.id"); 
		
		sb.append(" WHERE a.del_flag =? and rm.role_id=? ");
		
		sb.append(" ORDER BY a.sort,a.update_date desc ");
		
		params.add(Constants.DEL_FLAG_NORMAL);
		params.add(roleId);
		return super.findBySql(sb.toString(), params, OfficeEo.class);
	}

}
