
package com.jad.core.sys.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.JadTreeServiceImpl;
import com.jad.commons.util.QoUtil;
import com.jad.core.sys.dao.OfficeDao;
import com.jad.core.sys.entity.OfficeEo;
import com.jad.core.sys.qo.OfficeQo;
import com.jad.core.sys.service.OfficeService;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.dao.utils.EntityUtils;

/**
 * 机构Service
 */
@Service("officeService")
@Transactional(readOnly = true)
public class OfficeServiceImpl extends JadTreeServiceImpl<OfficeEo,String,OfficeVo> implements OfficeService{
	
	@Autowired
	private OfficeDao officeDao;

	public List<OfficeVo> findList(Boolean isAll,String type){
//		TODO
		OfficeQo qo = QoUtil.newNormalDataQo(OfficeQo.class);
		if(StringUtils.isNotBlank(type)){
			qo.setType(type);
		}
		return findList(qo);
	}
	
	/**
	 * 跟据角色id查找部门
	 * @param roleId
	 * @return
	 */
	public List<OfficeVo> findByRoleId(String roleId) {
		return EntityUtils.copyToVoList(officeDao.findByRoleId(roleId), OfficeVo.class);
	}
	
	
}
