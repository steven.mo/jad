package com.jad.core.sys.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.jad.core.sys.dao.SysUserLoginDao;
import com.jad.core.sys.entity.SysUserLoginEo;
import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.annotation.JadDao;

@JadDao("sysUserLoginDao")
public class SysUserLoginDaoImpl extends AbstractJadEntityDao<SysUserLoginEo,Long>implements SysUserLoginDao{
	public Boolean checkToken(String token){
		String sql = "update sys_user_login set act_time=SYSDATE() where token=? and act_time > DATE_SUB(NOW(), INTERVAL 30 MINUTE)";
		List params=new ArrayList();
		params.add(token);
	
		return this.executeSql(sql,params)>=1;
	}
}
