package com.jad.core.sys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jad.commons.entity.TreeEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;


@Entity
@Table(name = "sys_area")
public class AreaEo extends TreeEo<AreaEo,String>{
	
	private static final long serialVersionUID = 1L;
	
	private AreaEo parent;	// 父级
	
	private String code; 	// 区域编码
	
	private String type; 	// 区域类型（1：国家；2：省份、直辖市；3：地市；4：区县）

	public AreaEo(){
		super();
	}

	public AreaEo(String id){
		super(id);
	}
	
	@JsonBackReference
	@NotNull
	@ManyToOne
	@RelateColumn(name="parent_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public AreaEo getParent() {
		return parent;
	}

	public void setParent(AreaEo parent) {
		this.parent = parent;
	}

	@Column(name="type")
	@Length(min=1, max=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name="code")
	@Length(min=0, max=100)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

	

}
