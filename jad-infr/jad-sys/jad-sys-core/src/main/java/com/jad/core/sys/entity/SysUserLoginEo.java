package com.jad.core.sys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jad.commons.entity.BaseEo;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.annotation.KeyGenStrategy;
import com.jad.dao.enums.IdType;

import java.util.Date;

@Entity
@Table(name = "sys_user_login")
@KeyGenStrategy(idType = IdType.AUTO)
public class SysUserLoginEo implements EntityObject {

	private static final long serialVersionUID = 1L;

	public SysUserLoginEo() {
		super();
	}

	@Id
	@Column(name = "id")
	private Integer id; // id

	@Column(name = "user_name")
	private String userName; // user_name

	@Column(name = "login_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date loginTime; // login_time

	@Column(name = "act_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date actTime;

	@Column(name = "token")
	private String token; // token

	public Date getActTime() {
		return actTime;
	}

	public void setActTime(Date actTime) {
		this.actTime = actTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
