package com.jad.commons.vo;

import java.io.Serializable;


public class UserDataVo<ID extends Serializable> extends DataVo<ID>{
	
	private static final long serialVersionUID = 1L;


	protected String createBy;	// 创建者
	
	protected String updateBy;	// 更新者
	
	protected String remarks;	// 备注
	
	public UserDataVo() {
	}
	
	public UserDataVo(ID id) {
		super(id);
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	
	
}
