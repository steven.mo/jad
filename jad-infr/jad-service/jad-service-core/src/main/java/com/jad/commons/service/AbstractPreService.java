package com.jad.commons.service;

import java.io.Serializable;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.context.Constants;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.ValueObject;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.utils.EntityUtils;

@Transactional(readOnly = true)
public abstract class AbstractPreService<EO extends EntityObject,ID extends Serializable> 
	implements BaseService{
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractPreService.class);
	
	public abstract ID genId();

//	protected <VO extends BaseVo<ID>> void preInsert(VO vo,EoMetaInfo<EO> eoMetaInfo) {
	@SuppressWarnings("rawtypes")
	protected <VO extends ValueObject> void preInsert(VO vo,EoMetaInfo<EO> eoMetaInfo) {
		if(vo==null)return;
		if(vo instanceof BaseVo){
//			if(((BaseVo)vo).getId() ==null || StringUtils.isBlank(((BaseVo)vo).getId().toString())){
				((BaseVo)vo).setId(genId());
//			}
			
			if(((BaseVo)vo).getCreateDate() == null ){
				((BaseVo)vo).setCreateDate(new Date());	
			}
			if(((BaseVo)vo).getUpdateDate() == null ){
				((BaseVo)vo).setUpdateDate(new Date());
			}
			if(((BaseVo)vo).getDelFlag()==null){
				((BaseVo)vo).setDelFlag(Constants.DEL_FLAG_NORMAL);
			}
			
		}else if(eoMetaInfo.getKeyFieldInfo()!=null){
			String keyField=eoMetaInfo.getKeyFieldInfo().getFieldName();//主键字段名
			VoMetaInfo voMetaInfo = EntityUtils.getVoInfo(vo.getClass());
			if(voMetaInfo.getFieldInfoMap().get(keyField)!=null){
				
//				if(ReflectionUtils.getFieldValue(vo, keyField)==null){
					
					ReflectionUtils.setFieldValue(vo, keyField, genId());//整个主建进去
					
//				}
			}else{
				logger.warn(voMetaInfo.getObjName()+"中没有字段:"+keyField);
			}
		}else{
			logger.warn(eoMetaInfo.getObjName()+"中没有主键字段");
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	protected <VO extends ValueObject> void preUpdate(VO vo) {
		if(vo==null)return;
		if(vo instanceof BaseVo){
			if(((BaseVo)vo).getUpdateDate() == null ){
				((BaseVo)vo).setUpdateDate(new Date());
			}
		}
		
		
	}
	
	
}
