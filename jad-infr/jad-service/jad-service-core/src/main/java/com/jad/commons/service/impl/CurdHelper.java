package com.jad.commons.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.google.gson.Gson;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.DelStrategy;
import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.service.AbstractPreService;
import com.jad.commons.service.ServiceException;
import com.jad.commons.util.QoUtil;
import com.jad.commons.utils.IdGen;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.BaseVo;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.commons.vo.QueryObject;
import com.jad.commons.vo.ValueObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.VoMetaInfo;
import com.jad.dao.enums.DBType;
import com.jad.dao.enums.IdType;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;


/**
 * 抽象service
 * 
 * 只做一些依赖检查，泛型处理等操作
 * 
 * @author hechuan
 *
 * @param <EO>
 * @param <ID>
 * @param <VO>
 */
public class CurdHelper <EO extends EntityObject, ID extends Serializable,VO extends ValueObject>
	extends AbstractPreService<EO,ID>  {
	
	private static final Logger logger = LoggerFactory.getLogger(CurdHelper.class);

	private EoMetaInfo<EO> eoMetaInfo;	
	
	private VoMetaInfo<VO> voMetaInfo;
	
	private JadEntityDao<EO,ID> dao;
	
	/**
	 * 生成id
	 * @return
	 */
	public ID genId(){
		
		String dbType=Global.getDbType();
		
		if(IdType.AUTO.getKey()==eoMetaInfo.getIdType().getKey()){
			
			if(DBType.ORACLE.getDb().equals(dbType)){
				
//				String sequenceName=StringUtils.isBlank(eoMetaInfo.getSequenceName())?
//						Constants.DEFAULT_ID_GEN_SEQ:eoMetaInfo.getSequenceName();
				if(StringUtils.isBlank(eoMetaInfo.getSequenceName())){
					eoMetaInfo.setSequenceName(eoMetaInfo.getTableName().toUpperCase()+"_SEQ");
				}
				String sequenceName=eoMetaInfo.getSequenceName();
				
				return genOracleSeq(sequenceName);
				
			}else{
				
				return null;
			}
			
		}else if(IdType.UUID.getKey()==eoMetaInfo.getIdType().getKey()){
			return (ID)IdGen.uuid();
			
		}else if(IdType.SEQUENCE.getKey()==eoMetaInfo.getIdType().getKey()){
			
			if(DBType.ORACLE.getDb().equals(dbType)){
				
//				String sequenceName=StringUtils.isBlank(eoMetaInfo.getSequenceName())?
//						Constants.DEFAULT_ID_GEN_SEQ:eoMetaInfo.getSequenceName();
				if(StringUtils.isBlank(eoMetaInfo.getSequenceName())){
					eoMetaInfo.setSequenceName(eoMetaInfo.getTableName().toUpperCase()+"_SEQ");
				}
				String sequenceName=eoMetaInfo.getSequenceName();
				
				return genOracleSeq(sequenceName);
				
			}else{
//				logger.warn("数据库类型["+dbType+"]不支持序列,eoMetaInfo:"+eoMetaInfo.getObjName());
				return (ID)IdGen.uuid();
			}
		}else{
			logger.warn("类型的主键类型,eoMetaInfo:"+eoMetaInfo.getObjName());
			return (ID)IdGen.uuid();
		}
	}
	
	private ID genOracleSeq(String seqName){
		String alias="SEQ";
		List<Map<String,?>> list = getDao().findMapBySql(SqlHelper.getSequenceSql(seqName,alias), new ArrayList());
		if(list==null || list.isEmpty()){
			throw new ServiceException("无法取到序列["+seqName+"]的值");
		}
		if(eoMetaInfo.getKeyFieldInfo()==null || eoMetaInfo.getKeyFieldInfo().getFieldType()==null){
			logger.warn("无法识别实体["+eoMetaInfo.getObjName()+"]的主键信息");
			return (ID)list.get(0).get(alias).toString();
		}
		if(String.class.equals(eoMetaInfo.getKeyFieldInfo().getFieldType())){
			return (ID)list.get(0).get(alias).toString();
		}else if(Integer.class.equals(eoMetaInfo.getKeyFieldInfo().getFieldType())){
			return (ID)Integer.valueOf(list.get(0).get(alias).toString());
		}else if(Long.class.equals(eoMetaInfo.getKeyFieldInfo().getFieldType())){
			return (ID)Long.valueOf(list.get(0).get(alias).toString());
		}else{
			logger.warn("实体["+eoMetaInfo.getObjName()+"]的主键["
						+eoMetaInfo.getKeyFieldInfo().getFieldName()
					+"]类型["+eoMetaInfo.getKeyFieldInfo().getFieldType().getName()+"]不规范");
			return (ID)list.get(0).get(alias).toString();
		}
	}
	
	public VO findById(ID id) {
		Assert.notNull(id);
		EO eo = getDao().findById(id);
		if(eo==null){
			return null;
		}
		return toVo(eo);
	}
	
	public List<VO> findByIdList(List<ID> idList){
		Assert.notEmpty(idList);
		return findByIdList(idList,null);
	}
	
	public List<VO> findByIdList(List<ID> idList,String orderBy){
		Assert.notEmpty(idList);
		return EntityUtils.copyToVoList(getDao().findByIdList(idList,orderBy), voMetaInfo.getMetaClass());
	}

	public <QO extends QueryObject> List<VO> findList(QO qo) {
		return findList(qo,null);
	}
	
	public <QO extends QueryObject> VO findOne(QO qo){
		return findOne(qo,false);
	}
	
	public <QO extends QueryObject> VO findOne(QO qo,String orderBy){
		return findOne(qo,orderBy,false);
	}
	
	public <QO extends QueryObject> VO findOne(QO qo,boolean exceptionThenMore){
		return findOne(qo,null,exceptionThenMore);
	}
	
	public <QO extends QueryObject> VO findOne(QO qo,String orderBy,boolean exceptionThenMore){
		List<VO>voList=findList(qo,orderBy);
		if(voList!=null && voList.size()>0){
			if(voList.size()>1 && exceptionThenMore){
				logger.warn("数据重复:"+this.getEoMetaInfo().getTableName()+",qo:"+new Gson().toJson(qo));
				throw new ServiceException("读取到重复数据");
			}
			
			return voList.get(0);
		}
		
		return null;
	}
	
	public <QO extends QueryObject> List<VO> findAll(){
		return findList(QoUtil.newBaseNormalDataQo(),null);
	}
	public <QO extends QueryObject> List<VO> findAll(String orderBy){
		return findList(QoUtil.newBaseNormalDataQo(),orderBy);
	}
	
	
	public <QO extends QueryObject> List<VO> findList(QO qo,String orderBy) {
		Assert.notNull(qo);
		List<EO>list=getDao().findByQo(QoUtil.wrapperNormalDataQo(qo),orderBy);
		return EntityUtils.copyToVoList(list, voMetaInfo.getMetaClass());
	}
	
	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo,String orderBy) {
		
		Assert.notNull(page);
		Assert.notNull(qo);
		
		Page<EO>dataPage=getDao().findPageByQo(page, QoUtil.wrapperNormalDataQo(qo),orderBy);
		
		Page<VO>voPage=new Page<VO>(dataPage.getPageNo(),dataPage.getPageSize(),dataPage.getCount());
		
		voPage.setList(EntityUtils.copyToVoList(dataPage.getList(), voMetaInfo.getMetaClass()));
		
		return voPage;
	}

	public <QO extends QueryObject> Page<VO> findPage(PageQo page, QO qo) {
		
		return findPage(page,qo,null);
		
	}
	
	
	/**
	 * 增加 
	 * @param vo
	 */
	public void add(VO vo){
		Assert.notNull(vo);
		preInsert(vo,eoMetaInfo);
		EO eo = getDao().add(toEo(vo));
		ID id = getIdFromEo(eo);
		if(id!=null){
			ReflectionUtils.setFieldValue(vo, getEoMetaInfo().getKeyFieldInfo().getFieldName(), id);
		}
	}
	
	
	
	/**
	 * 更新
	 * @param vo
	 */
	public void update(VO vo){
		Assert.notNull(vo);
		preUpdate(vo);
		getDao().updateById(toEo(vo), getIdFromVo(vo));
	}
	
	public int updateId(ID newId,ID oldId){
		return getDao().updateId(newId, oldId);
	}
	
	private ID getIdFromVo(VO vo){
		ID id=(ID)ReflectionUtils.getFieldValue(vo, getEoMetaInfo().getKeyFieldInfo().getFieldName());
		return id;
	}
	
	private ID getIdFromEo(EO eo){
		ID id=(ID)ReflectionUtils.getFieldValue(eo, getEoMetaInfo().getKeyFieldInfo().getFieldName());
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public void delete(VO vo) {
		
		Assert.notNull(vo);
		
		ID id = getIdFromVo(vo);
		if(id!=null){
			if(DelStrategy.PHYSICAL.equals(Global.getDelStrategy()) || !(vo instanceof BaseVo)){
				getDao().deleteById(id);//物理删除
			}else {
				((BaseVo)vo).setDelFlag(Constants.DEL_FLAG_DELETE);
				this.update(vo);//逻辑删除
			}
		}else{
			VoMetaInfo<VO>voi=EntityUtils.getVoInfo((Class<VO>)vo.getClass());
			logger.warn(String.format("无法删除%s，末知id", voi.getObjName()) );
		}
		
		
		
	}
	

	
	public EO newEo(){
		try {
			
			return eoMetaInfo.getMetaClass().newInstance();
			
		} catch (Exception e) {
			
			throw new ServiceException(e);
			
		} 
		
	}
	
	public VO newVo(){
		
		try {
			
			return voMetaInfo.getMetaClass().newInstance();
			
		} catch (Exception e) {
			
			throw new ServiceException(e);
			
		} 
		
		
	}
	
	public EO toEo(VO vo){
		if(vo==null){
			return null;
		}
		return EntityUtils.copyToEo(vo, eoMetaInfo.getMetaClass());
	}
	
	public VO toVo(EO eo){
		if(eo==null){
			return null;
		}
		return EntityUtils.copyToVo(eo, voMetaInfo.getMetaClass());
	}
	
	
	
	
	
	

	public EoMetaInfo<EO> getEoMetaInfo() {
		return eoMetaInfo;
	}

	public void setEoMetaInfo(EoMetaInfo<EO> eoMetaInfo) {
		this.eoMetaInfo = eoMetaInfo;
	}

	public VoMetaInfo<VO> getVoMetaInfo() {
		return voMetaInfo;
	}

	public void setVoMetaInfo(VoMetaInfo<VO> voMetaInfo) {
		this.voMetaInfo = voMetaInfo;
	}

	public void setDao(JadEntityDao<EO, ID> dao) {
		this.dao = dao;
	}


	public JadEntityDao<EO, ID> getDao() {
		return dao;
	}
	

}



