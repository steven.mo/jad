package com.jad.dao.mybatis.mapper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.Configuration;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.enums.DBType;
import com.jad.dao.mybatis.enums.SqlMethod;
import com.jad.dao.utils.EntityUtils;


import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.apache.ibatis.executor.keygen.Jdbc3KeyGenerator;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.mybatis.enums.SqlMethod;
import com.jad.dao.utils.EntityUtils;

public class JadAutoSqlInjector implements ISqlInjector {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	protected Configuration configuration;

	protected LanguageDriver languageDriver;

	protected MapperBuilderAssistant builderAssistant;

//	protected DBType dbType = DBType.MYSQL;
	
	private static ISqlInjector sqlInjector;
	
	private static final CopyOnWriteArrayList<String> INJECT_CACHE = new CopyOnWriteArrayList<String>();

	
	public static ISqlInjector getSqlInjector() {
		if(sqlInjector==null){
			sqlInjector=new JadAutoSqlInjector();
		}
		return sqlInjector;
	}
	
	
	/**
	 * CRUD注入后给予标识 注入过后不再注入
	 *
	 * @param builderAssistant
	 * @param mapperClass
	 */
	public void inspectInject(MapperBuilderAssistant builderAssistant, Class<?> mapperClass) {
//		String className = mapperClass.toString();
		String className=mapperClass.getName();
//		Set<String> mapperRegistryCache = GlobalConfiguration.getMapperRegistryCache(builderAssistant.getConfiguration());
//		if (!mapperRegistryCache.contains(className)) {
		if (!INJECT_CACHE.contains(className) ) {
			inject(builderAssistant, mapperClass);
//			mapperRegistryCache.add(className);
			INJECT_CACHE.add(className);
		}
	}

	/**
	 * 注入单点 Sql
	 */
	public void inject(MapperBuilderAssistant builderAssistant, Class<?> mapperClass) {
		this.configuration = builderAssistant.getConfiguration();
		this.builderAssistant = builderAssistant;
		this.languageDriver = configuration.getDefaultScriptingLanuageInstance();
//		GlobalConfiguration globalCache = GlobalConfiguration.globalConfig(configuration);
//		this.dbType = globalCache.getDbType();
		/*
		 * 驼峰设置 PLUS 配置 > 原始配置
		 */
//		if (!globalCache.isDbColumnUnderline()) {
//			globalCache.setDbColumnUnderline(configuration.isMapUnderscoreToCamelCase());
//		}
//		Class<?> modelClass = extractModelClass(mapperClass);
//		if(modelClass==null){
//			logger.warn(String.format("无法从类%s中通过范型信息获得实体信息", mapperClass.getName()));
//			return;
//		}

//		TableInfo table = TableInfoHelper.initTableInfo(builderAssistant, modelClass);
		
//		EoMetaInfo table=EntityUtils.getEoInfo((Class<EntityObject>)modelClass);
		
		/**
		 * 没有指定主键，默认方法不能使用
		 */
//		if (table!=null && table.getKeyFieldInfo()!=null) {
			
			/*执行sql*/
			this.injectExecuteSql(mapperClass);
			
			this.injectInsertSql(mapperClass);//
			
			this.injectFindBySql(mapperClass);
			
			this.injectFindPageBySql(mapperClass);
//			
//		} else {
//			/**
//			 * 警告
//			 */
//			logger.warn(String.format("%s ,类无法自动注入SqlSource.",modelClass.toString()));
//		}
	}

//	
	
	protected void injectFindPageBySql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.FIND_PAGE_BY_SQL;
		String sql = sqlMethod.getSql();
//		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, null);
		
		this.addFindBySqlMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, null);
		
	}
	
	
	protected void injectFindBySql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.FIND_BY_SQL;
		String sql = sqlMethod.getSql();
//		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, null);
		
		this.addFindBySqlMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, null);
		
	}
	
	protected void injectExecuteSql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.EXECUTE_SQL;
		String sql = sqlMethod.getSql();
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		this.addUpdateMappedStatement(mapperClass,  sqlMethod.getMethod(), sqlSource);
	}
	
	protected void injectInsertSql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.INSERT_SQL;
		String sql = sqlMethod.getSql();
		this.addInsertMappedStatement(mapperClass,  sqlMethod.getMethod(), sql);
	}

	
	public MappedStatement addFindBySqlMappedStatement(Class<?> mapperClass, String id, SqlSource sqlSource, Class<?> resultType) {
		/* 普通查询 */
		return this.addMappedStatement(mapperClass, id, sqlSource, SqlCommandType.SELECT, null, null, resultType,
				new NoKeyGenerator(), null, null);
	}
	
	
	public MappedStatement addUpdateMappedStatement(Class<?> mapperClass,  String id, SqlSource sqlSource) {
		return this.addMappedStatement(mapperClass, id, sqlSource, 
				SqlCommandType.UPDATE, List.class, null, Integer.class,
				new NoKeyGenerator(), null, null);
	}
	
	/**
	 * 从接口中的范型对象中获得实体类
	 * @param mapperClass
	 * @return
	 */
	public static Class<EntityObject> extractEoClass(Class<?> mapperClass) {
		if(!JadEntityDao.class.isAssignableFrom(mapperClass)){
			return null;
		}
		Type[] types = mapperClass.getGenericInterfaces();
		ParameterizedType target = null;
		for (Type type : types) {
			if (type instanceof ParameterizedType && JadEntityDao.class.isAssignableFrom(mapperClass)) {
				target = (ParameterizedType) type;
				break;
			}
		}
		if(target==null){
			return null;
		}
		Type[] parameters = target.getActualTypeArguments();
		
		if(parameters==null || parameters.length==0){
			return null;
		}
		
		Class<EntityObject> modelClass = (Class<EntityObject>) parameters[0];
		return modelClass;
	}
	
	

	
	private MappedStatement addInsertMappedStatement(Class<?> mapperClass,  String id, String sql) {
		
		try {
			Class<EntityObject>entityClass=extractEoClass(mapperClass);
			if(entityClass!=null){
				EoMetaInfo<EntityObject> ei = EntityUtils.getEoInfo(entityClass);
				if(ei!=null){
					EoFieldInfo<EntityObject>keyField=ei.getKeyFieldInfo();
					if(keyField!=null && StringUtils.isNotBlank(keyField.getColumn()) && StringUtils.isNotBlank(keyField.getFieldName())){
						
//						Class<?> mapperClass, String id, SqlSource sqlSource,
//						SqlCommandType sqlCommandType, Class<?> parameterClass, String resultMap, Class<?> resultType,
//						KeyGenerator keyGenerator, String keyProperty, String keyColumn
						
						SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, ei.getMetaClass());
						
						return this.addMappedStatement(mapperClass, id, sqlSource, 
								SqlCommandType.INSERT, ei.getMetaClass(), null, Integer.class,
								new Jdbc3KeyGenerator(), keyField.getFieldName(), keyField.getColumn());
					}
				}
			}
				
		} catch (Exception e) {
			logger.error("自动注入["+mapperClass.getName()+"]中的insert方法时发生异常，这将导致数据插入数据库后无法直接返回所插入数据的注键值，原因:"+e.getMessage(),e);
		}
		
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		return this.addMappedStatement(mapperClass, id, sqlSource, 
				SqlCommandType.INSERT, List.class, null, Integer.class,
				new NoKeyGenerator(), null, null);
		
		
	}
	
	/*
	public MappedStatement addInsertMappedStatement(Class<?> mapperClass,  String id, String sql) {
		
		try {
			Class<EntityObject>entityClass=extractEoClass(mapperClass);
			if(entityClass!=null){
				EoMetaInfo<EntityObject> ei = EntityUtils.getEoInfo(entityClass);
				if(ei!=null){
					EoFieldInfo<EntityObject>keyField=ei.getKeyFieldInfo();
					if(keyField!=null && StringUtils.isNotBlank(keyField.getColumn()) && StringUtils.isNotBlank(keyField.getFieldName())){
//						return this.addMappedStatement(mapperClass, id, sqlSource, 
//								SqlCommandType.INSERT, List.class, null, Integer.class,
//								new Jdbc3KeyGenerator(), keyField.getFieldName(), keyField.getColumn());
						
//						Class<?> mapperClass, String id, SqlSource sqlSource,
//						SqlCommandType sqlCommandType, Class<?> parameterClass, String resultMap, Class<?> resultType,
//						KeyGenerator keyGenerator, String keyProperty, String keyColumn
						
						SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, ei.getMetaClass());
						
						return this.addMappedStatement(mapperClass, id, sqlSource, 
								SqlCommandType.INSERT, ei.getMetaClass(), null, Integer.class,
								new Jdbc3KeyGenerator(), EntityUtils.JAD_KEY_NAME, keyField.getColumn());
						
//						return this.addMappedStatement(mapperClass, id, sqlSource, 
//								SqlCommandType.INSERT, ei.getMetaClass(), null, Integer.class,
//								new Jdbc3KeyGenerator(), keyField.getFieldName(), keyField.getColumn());
						
					}
				}
			}
				
		} catch (Exception e) {
			logger.error("自动注入["+mapperClass.getName()+"]中的insert方法时发生异常，这将导致数据插入数据库后无法直接返回所插入数据的注键值，原因:"+e.getMessage(),e);
		}
		
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		return this.addMappedStatement(mapperClass, id, sqlSource, 
				SqlCommandType.INSERT, List.class, null, Integer.class,
				new NoKeyGenerator(), null, null);
		
		
	}
	*/
//	return this.addMappedStatement(mapperClass, id, sqlSource, 
//			SqlCommandType.INSERT, List.class, null, Integer.class,
//			new Jdbc3KeyGenerator(), EntityUtils.JAD_KEY_NAME, keyField.getColumn());
	
	public MappedStatement addMappedStatement(Class<?> mapperClass, String id, SqlSource sqlSource,
			SqlCommandType sqlCommandType, Class<?> parameterClass, String resultMap, Class<?> resultType,
			KeyGenerator keyGenerator, String keyProperty, String keyColumn) {
		String statementName = mapperClass.getName() + "." + id;
		if (configuration.hasStatement(statementName)) {
			System.err.println("{" + statementName
					+ "} Has been loaded by XML or SqlProvider, ignoring the injection of the SQL.");
			return null;
		}
		/* 缓存逻辑处理 */
		boolean isSelect = false;
		if (sqlCommandType == SqlCommandType.SELECT) {
			isSelect = true;
		}
        return builderAssistant.addMappedStatement(id, sqlSource, StatementType.PREPARED, sqlCommandType, null, null, null,
				parameterClass, resultMap, resultType, null, !isSelect, isSelect, false, keyGenerator, keyProperty, keyColumn,
				configuration.getDatabaseId(), languageDriver, null);
	}

}
