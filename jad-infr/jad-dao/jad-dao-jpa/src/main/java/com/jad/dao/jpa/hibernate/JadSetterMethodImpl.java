package com.jad.dao.jpa.hibernate;

import java.lang.reflect.Method;

import org.hibernate.PropertySetterAccessException;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.property.access.spi.SetterMethodImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.vo.PojoObject;
import com.jad.dao.enums.JavaType;
import com.jad.dao.utils.EntityUtils;

public class JadSetterMethodImpl extends SetterMethodImpl{
	
	private static final Logger LOGGER=LoggerFactory.getLogger(JadSetterMethodImpl.class);
	
	/**
	 * 是否为关联实体
	 */
	private boolean isEntity=false;
	
	
	/**
	 * 关联实体属性名
	 */
	private String relatePropertyName;
	
	/**
	 * 关联实体属性的 setter方法
	 */
	private JadSetterMethodImpl relateSetterMethod;
	
	/**
	 * 关联实体属性的 getter方法
	 */
	private JadGetterMethodImpl relateGetterMethod;
	
	/**
	 * 该setter方法的参数类型
	 */
	private Class<?> parameterClass;
	
	private JavaType parameterType;
	
	/**
	 * 属性名
	 */
	private final String propertyName;
	
	private final Class containerClass;
	
	/**
	 * 属性对像的getter方法
	 */
	private final JadGetterMethodImpl getter;
	
	public JadSetterMethodImpl(Class containerClass, String propertyName, Method setterMethod,
			JadGetterMethodImpl getter) {
		
		super(containerClass,propertyName,setterMethod);
		
		this.containerClass=containerClass;
		this.propertyName=propertyName;
		this.getter=getter;
		
		Class[]parameterTypes=setterMethod.getParameterTypes();
		
		if(parameterTypes!=null && parameterTypes.length==1){
			
			parameterClass=parameterTypes[0];
			parameterType=JavaType.getJavaType(parameterClass.getName());
			
			if(EntityUtils.isRelateProperty(propertyName) 
					&& JavaType.Object.getType().equals(parameterType.getType()) 
					&& PojoObject.class.isAssignableFrom(parameterClass)  
					){
				
				isEntity=true;
//				relateName=EoInfoUtils.getRelateEntityName(propertyName);
				relatePropertyName=EntityUtils.getRelatePropertyName(propertyName);
				
				String getProtpertyName=relatePropertyName;
				if(EntityUtils.isRelateProperty(getProtpertyName)){
					getProtpertyName = EntityUtils.getRelateEntityName(getProtpertyName);
				}
				
				Method relaGet=ReflectionUtils.findGetterMethod(parameterClass, getProtpertyName);
				relateGetterMethod=new JadGetterMethodImpl(parameterClass,relatePropertyName,relaGet);
				
				Method relaSet=ReflectionUtils.findSetterMethod(parameterClass, getProtpertyName, relateGetterMethod.getReturnType());
				relateSetterMethod=new JadSetterMethodImpl(parameterClass,relatePropertyName,relaSet,relateGetterMethod);
				
			}
		}
	}
	
	
	public void set(Object target, Object value, SessionFactoryImplementor factory) {
		
		if(value==null){
			return;
		}
		if(!isEntity && JavaType.Object.getType().equals(parameterType.getType()) ){
			super.set(target, value, factory);
			return;
		}
		
		if(isEntity){
			setEntity(target, value, factory);
			return;
		}
		
		if( JavaType.isBool(parameterType.getType()) 
				&& (!Boolean.class.isAssignableFrom(value.getClass()))){
			
			setBoolean(target, value, factory);
			
			return;
		}
		
		if( JavaType.isChar(parameterType.getType()) 
				&& (!Character.class.isAssignableFrom(value.getClass()))){
			
			setChar(target, value, factory);
			
			return;
		}
		
		if( JavaType.isByte(parameterType.getType()) 
				&& (!Byte.class.isAssignableFrom(value.getClass()))){
			
			setByte(target, value, factory);
			
			return;
		}
		
		if( JavaType.isShort(parameterType.getType()) 
				&& (!Short.class.isAssignableFrom(value.getClass()))){
			
			setShort(target, value, factory);
			
			return;
		}
		
		
		if( JavaType.isInt(parameterType.getType()) 
				&& (!Integer.class.isAssignableFrom(value.getClass()))){
			
			setInt(target, value, factory);
			
			return;
		}
		
		if( JavaType.isLong(parameterType.getType()) 
				&& (!Long.class.isAssignableFrom(value.getClass()))){
			
			setLong(target, value, factory);
			
			return;
		}
		
		
		if( JavaType.isFloat(parameterType.getType()) 
				&& (!Float.class.isAssignableFrom(value.getClass()))){
			
			setFloat(target, value, factory);
			
			return;
		}
		
		if( JavaType.isDouble(parameterType.getType()) 
				&& (!Double.class.isAssignableFrom(value.getClass()))){
			
			setDouble(target, value, factory);
			
			return;
		}
		
		
		if(JavaType.Calendar.getType().equalsIgnoreCase(parameterType.getType()) 
				&& java.util.Date.class.isAssignableFrom(value.getClass())  ){
			
			
			java.util.Calendar timeVal=java.util.Calendar.getInstance();
			
			timeVal.setTime((java.util.Date)value);
			
			super.set(target, timeVal, factory);
			return;
			
		}
		
		if(JavaType.utilDate.getType().equalsIgnoreCase(parameterType.getType()) 
				&& java.util.Date.class.isAssignableFrom(value.getClass()) ){
			
			java.util.Date timeVal=(java.util.Date)value;
			super.set(target, timeVal, factory);
			return;
			
		}
		if(JavaType.sqlDate.getType().equalsIgnoreCase(parameterType.getType()) 
				&& java.sql.Date.class.isAssignableFrom(value.getClass())  ){
			
			java.sql.Date timeVal=(java.sql.Date)value;
			super.set(target, timeVal, factory);
			return;
			
		}
		if(JavaType.sqlTime.getType().equalsIgnoreCase(parameterType.getType()) 
				&& java.sql.Time.class.isAssignableFrom(value.getClass()) ){
			
			java.sql.Time timeVal=(java.sql.Time)value;
			super.set(target, timeVal, factory);
			return;
			
		}
		if(JavaType.Timestamp.getType().equalsIgnoreCase(parameterType.getType()) 
				&& java.sql.Timestamp.class.isAssignableFrom(value.getClass()) ){
			
			java.sql.Timestamp timeVal=(java.sql.Timestamp)value;
			super.set(target, timeVal, factory);
			return;
		}
		
		if(JavaType.String.getType().equalsIgnoreCase(parameterType.getType()) 
				&& Character.class.isAssignableFrom(value.getClass()) ){
			
			String valStr=value.toString();
			super.set(target, valStr, factory);
			return;
		}
		
		super.set(target, value, factory);
		
	}
	
	
	public void setEntity(Object target, Object value, SessionFactoryImplementor factory) {
		try {
			Object relateEntity=getter.get(target);
			
			Class<?> getterContainerClass=getter.getReturnType();
			
			if(relateEntity==null){
				relateEntity=getterContainerClass.newInstance();
				super.set(target, relateEntity, factory);//整进去
			}
			
			relateSetterMethod.set(relateEntity, value, factory);
			
		}  catch (Exception e) {
			
			throw new PropertySetterAccessException(
					e,
					containerClass,
					propertyName,
					parameterClass,
					target,
					value
			);
			
		}
	}
	
	
	
	private void setFloat(Object target, Object value, SessionFactoryImplementor factory){
		if(value==null || "".equals(value.toString().trim())){
			if(JavaType.Float.getType().equals(parameterType.getType())){
				super.set(target, null, factory);
			}else if(JavaType.floatType.getType().equals(parameterType.getType())){
				Float val=(float)0;
				super.set(target, val, factory);
			}
			return ;
		}
		try {
			Float val=Float.parseFloat(value.toString());
			super.set(target, val, factory);
		} catch (NumberFormatException e) {
			LOGGER.error("自定义数据类型转换失败，"+e.getMessage(),e);
			super.set(target, value, factory);
		}
		
	}

	private void setDouble(Object target, Object value, SessionFactoryImplementor factory){
	
		if(value==null || "".equals(value.toString().trim())){
			if(JavaType.Double.getType().equals(parameterType.getType())){
				super.set(target, null, factory);
			}else if(JavaType.doubleType.getType().equals(parameterType.getType())){
				Double val=(double)0;
				super.set(target, val, factory);
			}
			return ;
		}
		
		try {
			Double val=Double.parseDouble(value.toString());
			super.set(target, val, factory);
		} catch (NumberFormatException e) {
			LOGGER.error("自定义数据类型转换失败，"+e.getMessage(),e);
			super.set(target, value, factory);
		}
	
	}
	
	private void setLong(Object target, Object value, SessionFactoryImplementor factory){
		
		if(value==null || "".equals(value.toString().trim())){
			if(JavaType.Long.getType().equals(parameterType.getType())){
				super.set(target, null, factory);
			}else if(JavaType.longType.getType().equals(parameterType.getType())){
				Long val=(long)0;
				super.set(target, val, factory);
			}
			return ;
		}
		
		try {
			Long val=Long.parseLong(value.toString());
			super.set(target, val, factory);
		} catch (NumberFormatException e) {
			LOGGER.error("自定义数据类型转换失败，"+e.getMessage(),e);
			super.set(target, value, factory);
		}
		
	}


	private void setInt(Object target, Object value, SessionFactoryImplementor factory){
		
		if(value==null || "".equals(value.toString().trim())){
			if(JavaType.Integer.getType().equals(parameterType.getType())){
				super.set(target, null, factory);
			}else if(JavaType.intType.getType().equals(parameterType.getType())){
				Integer intVal=(int)0;
				super.set(target, intVal, factory);
			}
			return ;
		}
		
		try {
			Integer intVal=Integer.parseInt(value.toString());
			super.set(target, intVal, factory);
		} catch (NumberFormatException e) {
			LOGGER.error("自定义数据类型转换失败，"+e.getMessage(),e);
			super.set(target, value, factory);
		}
		
	}

	private void setShort(Object target, Object value, SessionFactoryImplementor factory){
		
		if(value==null || "".equals(value.toString().trim())){
			if(JavaType.Short.getType().equals(parameterType.getType())){
				super.set(target, null, factory);
			}else if(JavaType.shortType.getType().equals(parameterType.getType())){
				Short shortVal=(short)0;
				super.set(target, shortVal, factory);
			}
			return ;
		}
		
		try {
			Short shortVal=Short.parseShort(value.toString());
			super.set(target, shortVal, factory);
		} catch (NumberFormatException e) {
			LOGGER.error("自定义数据类型转换失败，"+e.getMessage(),e);
			super.set(target, value, factory);
		}
		
	}
	


	private void setByte(Object target, Object value, SessionFactoryImplementor factory){
		
		if(value==null || "".equals(value.toString().trim())){
			if(JavaType.Byte.getType().equals(parameterType.getType())){
				super.set(target, null, factory);
			}else if(JavaType.byteType.getType().equals(parameterType.getType())){
				Byte byteVal=(byte)0;
				super.set(target, byteVal, factory);
			}
			return ;
		}
		
		try {
			Byte valBype=Byte.parseByte(value.toString());
			super.set(target, valBype, factory);
		} catch (NumberFormatException e) {
			LOGGER.error("自定义数据类型转换失败，"+e.getMessage(),e);
			super.set(target, value, factory);
		}
		
	}
	
	private void setChar(Object target, Object value, SessionFactoryImplementor factory){
		String valStr=value.toString();
		if(valStr.length()!=1){
			super.set(target, value, factory);
		}else{
			Character c=valStr.charAt(0);
			super.set(target, c, factory);
		}
	}
	
	private void setBoolean(Object target, Object value, SessionFactoryImplementor factory){
		
		String valStr=value.toString();
		
		if( "1".equalsIgnoreCase(valStr)  
				|| "true".equalsIgnoreCase(valStr) || "yes".equalsIgnoreCase(valStr) 
				|| "t".equalsIgnoreCase(valStr) || "y".equalsIgnoreCase(valStr) ){
			
			Boolean booleanVal=true;
			
			super.set(target, booleanVal, factory);
			
		}else if("0".equalsIgnoreCase(valStr) 
				|| "false".equalsIgnoreCase(valStr) || "no".equalsIgnoreCase(valStr) 
				|| "f".equalsIgnoreCase(valStr) || "n".equalsIgnoreCase(valStr)  ){
			
			Boolean booleanVal=false;
			
			super.set(target, booleanVal, factory);
			
		}else{
			super.set(target, value, factory);
		}
		
	}


}
