package com.jad.dao.jpa.support;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.Page;
import com.jad.dao.RootDao;
import com.jad.dao.entity.EoFieldVal;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.utils.DaoReflectionUtil;
import com.jad.dao.utils.EntityUtils;
import com.jad.dao.utils.SqlHelper;

public class JpaDaoDelegate implements  RootDao{

	private static Logger logger=LoggerFactory.getLogger(JpaDaoDelegate.class);
	

	private EntityManager entityManager;
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}


	@Override
	public <E> List<E> findBySql(Page<?> page, String sql, List<?> params,
			Class<E> entityClass) {
		return JpaDaoHelper.findBySql(entityManager, page, sql, params, entityClass);
	}
	


	@Override
	public int executeSql(String sql, List<?> params) {
		return JpaDaoHelper.executeSql(entityManager, sql, params);
	}
	
	
//	@Override
//	public int insertSql(String sql, List<?> params,Object retId) {
////		TODO 待完善，可能需要返回主键
//		return executeSql(sql,params);
//	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <E extends EntityObject>int insert(E e){
		Class entityClass = e.getClass();
		EoMetaInfo ei=EntityUtils.getEoInfo(entityClass);
		List<EoFieldVal<E>>valList=DaoReflectionUtil.getEntityFieldVals(e,entityClass);
		if(valList.isEmpty()){
			logger.warn("entity 所有可持久化属性都是空的,entityClass:"+entityClass.getName());
			return 0;
		}
		List params=new ArrayList();
		String addSql=SqlHelper.getAddSql(ei, valList,params);
		return this.executeSql(addSql, params);
	}
	
	
	
	
}
