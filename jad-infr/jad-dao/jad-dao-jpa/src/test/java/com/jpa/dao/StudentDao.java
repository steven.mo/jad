package com.jpa.dao;

import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jpa.entity.Student;

//@JadDao("studentDao")
@JadDao
public interface StudentDao extends JadEntityDao<Student, String> {


}
