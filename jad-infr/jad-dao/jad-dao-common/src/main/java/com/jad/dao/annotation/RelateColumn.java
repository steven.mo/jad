package com.jad.dao.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.dao.enums.RelateLoadMethod;

/**
 * 当实体的某一属性关性关联到一个对象时用此注解
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD})
public @interface RelateColumn {

	/**
	 * 列名
	 */
	String name();
	
	/**
	 * 关联表别名，默认跟属性名相同 
	 * @return
	 */
	String alia() default "";

	/**
	 * 加载方式
	 * 
	 * @return
	 */
	RelateLoadMethod loadMethod() default RelateLoadMethod.NOT_AUTO;

	boolean nullable() default true;

	boolean insertable() default true;

	boolean updatable() default true;

	
	int length() default 255;

    int precision() default 0;

    int scale() default 0;

}
