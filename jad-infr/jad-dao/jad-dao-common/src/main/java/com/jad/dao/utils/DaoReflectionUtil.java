package com.jad.dao.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.jad.commons.reflection.ReflectionUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.commons.vo.QueryObject;
import com.jad.dao.entity.EoFieldInfo;
import com.jad.dao.entity.EoFieldVal;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.entity.QoFieldInfo;
import com.jad.dao.entity.QoFieldVal;
import com.jad.dao.entity.QoMetaInfo;

public class DaoReflectionUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(DaoReflectionUtil.class);
	
	
	@SuppressWarnings("unchecked")
	public static <EO extends EntityObject> List<EoFieldVal<EO>> getEntityFieldVals(EO entity){
		Assert.notNull(entity);
		return getEntityFieldVals(entity,(Class<EO>)entity.getClass());
	}
	
	
	public static <EO extends EntityObject> List<EoFieldVal<EO>> getEntityFieldVals(EO entity,Class<EO> clazz){
		
		List<EoFieldVal<EO>>valList=new ArrayList<EoFieldVal<EO>>();
		
		EoMetaInfo<EO> ei = EntityUtils.getEoInfo(clazz);
		
		for(EoFieldInfo<EO> efi:ei.getFieldInfoMap().values()){
			
			Object val=ReflectionUtils.getFieldValue(entity, efi.getFieldName());
			
//			if(val != null){
				
				EoFieldVal<EO> valo=new EoFieldVal<EO>(efi,val);
				valList.add(valo);
				
//			}
			
			
		}
		return valList;
	}
	
	
	@SuppressWarnings("unchecked")
	public static <QO extends QueryObject> List<QoFieldVal<QO>> getQoFieldVals(QO qo){
		Assert.notNull(qo);
		
		return getQoFieldVals(qo,(Class<QO>)qo.getClass());
	}
	
	public static <QO extends QueryObject> List<QoFieldVal<QO>> getQoFieldVals(QO qo,Class<QO> clazz){
		
		List<QoFieldVal<QO>>valList=new ArrayList<QoFieldVal<QO>>();
		
		QoMetaInfo<QO> ei = EntityUtils.getQoInfo(clazz);
		
		for(QoFieldInfo<QO> efi:ei.getFieldInfoMap().values()){
			
			Object val = ReflectionUtils.getFieldValue(qo, efi.getFieldName());
//			if(val != null){
				QoFieldVal<QO> valo=new QoFieldVal<QO>(efi,val);
				valList.add(valo);
//			}
			
			
		}
		return valList;
	}
	
	/**
	 * 获取实体类的所有属性列表
	 *
	 * @param clazz
	 *            反射类
	 * @return
	 */
	public static Set<Field> getClassFields(Class<?> clazz) {
		Set<Field> result = new LinkedHashSet<Field>();
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {

			/* 过滤静态属性 */
			if (Modifier.isStatic(field.getModifiers())) {
				continue;
			}

			/* 过滤 transient关键字修饰的属性 */
			if (Modifier.isTransient(field.getModifiers())) {
				continue;
			}

			/* 过滤注解非表字段属性 */
			Transient annotation = field.getAnnotation(Transient.class);
			if (annotation != null) {
				continue;
			}
			result.add(field);
		}

		/* 处理父类方法 */
		Class<?> superClass = clazz.getSuperclass();
		if (superClass.equals(Object.class)) {
			return result;
		}
		result.addAll(getClassFields(superClass));
		return result;
	}
	
	
	
	
}


