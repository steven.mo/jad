package com.jad.dao.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *	
 *	如果value为true,而且实体中被此标注的属性值为null，
 *  则数据库中对应的字段值会被清空为null，返之不更新对应字段
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD})
public @interface ForceUpdateNull {
	
	boolean value() default false;
	
}
