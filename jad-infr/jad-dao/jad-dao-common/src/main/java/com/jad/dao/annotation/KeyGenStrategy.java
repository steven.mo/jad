package com.jad.dao.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jad.commons.context.Constants;
import com.jad.dao.enums.IdType;


/**
 * 主键生成策略
 * @author hechuan
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE,METHOD, FIELD})
public @interface KeyGenStrategy {
	
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emailSeq")
//	@SequenceGenerator(initialValue = 1, name = "emailSeq", sequenceName = "EMAIL_SEQUENCE")

	IdType idType() default IdType.UUID;
	
	String sequenceName()default Constants.DEFAULT_ID_GEN_SEQ;
	
}





