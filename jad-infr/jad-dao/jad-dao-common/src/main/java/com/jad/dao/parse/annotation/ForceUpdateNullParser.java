package com.jad.dao.parse.annotation;

import java.lang.reflect.Field;

import javax.persistence.Column;

import com.jad.commons.utils.StringUtils;
import com.jad.commons.vo.EntityObject;
import com.jad.dao.annotation.ForceUpdateNull;
import com.jad.dao.entity.EoFieldInfo;

public class ForceUpdateNullParser extends AbstractEoFieldAnnotationParser<ForceUpdateNull>{
	
	@Override
	public <EO extends EntityObject> boolean parseAnnotationEoField(
			EoFieldInfo<EO> eoFieldInfo, Field field, ForceUpdateNull annotation) {
		
		eoFieldInfo.setForceUpdateNull(annotation.value());
		
		return true;
	}
	
	

	
	


}
