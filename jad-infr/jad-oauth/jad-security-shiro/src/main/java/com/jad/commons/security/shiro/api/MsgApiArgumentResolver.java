package com.jad.commons.security.shiro.api;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jad.commons.annotation.JsonArg;
import com.jad.commons.json.JsonMapper;
import com.jad.commons.validator.annotation.MsgAnnValidateException;
import com.jad.commons.validator.annotation.MsgValidable;
import com.jad.commons.web.api.MsgRespCode;
import com.jad.commons.web.api.req.MsgReq;
import com.jad.commons.web.api.req.MsgReqHead;
import com.jad.commons.web.api.util.HttpRequestUtil;


/*
 <mvc:annotation-driven>
       <mvc:argument-resolvers>
        <bean class="com.jad.web.mvc.resolver.JsonArgumentResolver" />
    </mvc:argument-resolvers>
 </mvc:annotation-driven>
 */
public class MsgApiArgumentResolver implements HandlerMethodArgumentResolver {


	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		parameter.hasParameterAnnotations();
		return parameter.hasParameterAnnotation(JsonArg.class);
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public Object resolveArgument(final MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
			
		try {
			
			String body = getRequestBody(webRequest);
			
			HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);
			String needUnpack = (String)servletRequest.getAttribute(HttpRequestUtil.DATA_NEED_UNPACK);
			if(needUnpack!=null || ApiFilter.needUnpack(body) ){
				
				final GsonBuilder gsonBuilder = new GsonBuilder(); 
//				gsonBuilder.registerTypeAdapter(Book.class, new BookTypeAdapter()); 
				final Gson gson = gsonBuilder.create();

//				ObjectMapper mapper =MsgJsonMapper.getReqBodyObjectMapper(parameter.getParameterType());
				ObjectMapper mapper = null;
				MsgReq req = mapper.readValue(body, MsgReq.class);
				
				servletRequest.setAttribute(HttpRequestUtil.JSON_REQUEST_HEAD,getHead(req));//保存报文头
				
				req.validate();//校验报文
				
				if(req.getData()==null){
					return parameter.getParameterType().newInstance();
				}
				
				return req.getData();
				
			}else{
				Object val=JsonMapper.fromJsonString(body, parameter.getParameterType());
				if(val!=null && (val instanceof MsgValidable)){
					((MsgValidable)val).validate();//校验报文
				}
				return val;
			}
			
		}
		catch(MsgAnnValidateException e){
			throw new ApiHandlerException(MsgRespCode.PARAM_FAIL,e.getMessage(),e);//报文异常
		}
		catch (Exception e) {
			throw new ApiHandlerException(MsgRespCode.PARAM_FAIL,"报文无法识别",e);//报文异常
		}
		
		
	}
	
	@SuppressWarnings("rawtypes")
	public static MsgReqHead getHead(MsgReq req){
		MsgReqHead head=new MsgReqHead();
		head.setTrxCode(req.getTrxCode());
		head.setReqTime(req.getReqTime());
		head.setVer(req.getVer());
		head.setDeviceInfo(req.getDeviceInfo());
		head.setDataType(req.getDataType());
		head.setSn(req.getSn());
		head.setToken(req.getToken());
		return head;
	}

	private String getRequestBody(NativeWebRequest webRequest) throws Exception{
		HttpServletRequest servletRequest = webRequest.getNativeRequest(HttpServletRequest.class);

		String jsonBody = (String) webRequest.getAttribute(HttpRequestUtil.JSON_REQUEST_BODY,NativeWebRequest.SCOPE_REQUEST);
		
		if (jsonBody == null) {
			
			jsonBody = IOUtils.toString(servletRequest.getInputStream());
			webRequest.setAttribute(HttpRequestUtil.JSON_REQUEST_BODY, jsonBody,NativeWebRequest.SCOPE_REQUEST);
			
		}
		return jsonBody;
	}

}
