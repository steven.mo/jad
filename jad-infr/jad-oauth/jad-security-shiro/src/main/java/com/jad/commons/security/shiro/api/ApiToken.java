package com.jad.commons.security.shiro.api;

import org.apache.shiro.authc.AuthenticationToken;

public class ApiToken implements AuthenticationToken{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String token;
	
	

	public ApiToken(String token) {
		this.token = token;
	}

	@Override
	public Object getPrincipal() {
		return getToken();
	}
	@Override
	public Object getCredentials() {
		return getToken();
	}


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
	



}
