/**
 */
package com.jad.commons.security.shiro;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;

import com.jad.commons.utils.StringUtils;

/**
 * 系统安全认证实现类
 */
public class CacheSessionDAO extends EnterpriseCacheSessionDAO implements SessionDAO {


    /**
   	 * 获取活动会话
   	 * @param includeLeave 是否包括离线（最后访问时间大于3分钟为离线会话）
   	 * @param principal 根据登录者对象获取活动会话
   	 * @param excludeSessionId 不为空，则过滤掉（不包含）这个会话。
   	 * @return
   	 */
   	@Override
   	public Collection<Session> getActiveSessions(Object principal, String excludeSessionId) {
   		
	   	// 登录者为空时查所有会话
		if (principal == null){
			return getActiveSessions();
		}
   		
		Set<Session> sessions =  new HashSet();
		for (Session session : getActiveSessions()){
			boolean isActiveSession = false;
			// 符合登陆者条件。
			if (principal != null && (principal instanceof Principal ) ){
				
				PrincipalCollection pc = (PrincipalCollection)session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
				String id = ((Principal)principal).getId();
				
				if(id!=null && pc != null && pc.getPrimaryPrincipal()!=null 
						&& (pc.getPrimaryPrincipal() instanceof Principal) 
						&& id.equals(((Principal)pc.getPrimaryPrincipal()).getId()) ){
					
						isActiveSession = true;
				}
				
			}
			// 过滤掉的SESSION
			if(StringUtils.isNotBlank(excludeSessionId) && excludeSessionId.equals(session.getId() )){
				isActiveSession = false;
			}
			if (isActiveSession){
				sessions.add(session);
			}
		}
		return sessions;
   	}
    
	
}
