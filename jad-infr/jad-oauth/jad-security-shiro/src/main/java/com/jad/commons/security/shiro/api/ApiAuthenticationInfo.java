package com.jad.commons.security.shiro.api;

import org.apache.shiro.authc.SaltedAuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.ByteSource;

public class ApiAuthenticationInfo implements SaltedAuthenticationInfo{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PrincipalCollection principals;
	protected Object credentials;
	protected ByteSource credentialsSalt;
	
	public ApiAuthenticationInfo(Object principal, Object credentials, String realmName) {
		this.principals = new SimplePrincipalCollection(principal, realmName);
		this.credentials = credentials;
	}
	
	public ApiAuthenticationInfo(Object principal, Object credentials,ByteSource credentialsSalt, String realmName) {
		this.principals = new SimplePrincipalCollection(principal, realmName);
		this.credentials = credentials;
		this.credentialsSalt = credentialsSalt;
	}
	
	@Override
	public PrincipalCollection getPrincipals() {
		return principals;
	}

	@Override
	public Object getCredentials() {
		return credentials;
	}

	@Override
	public ByteSource getCredentialsSalt() {
		return credentialsSalt;
	}


}




