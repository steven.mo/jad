package com.jad.commons.security.shiro.api;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.jad.commons.json.JsonMapper;
import com.jad.commons.web.api.MsgRespCode;
import com.jad.commons.web.api.req.MsgReqHead;
import com.jad.commons.web.api.resp.MsgResp;
import com.jad.commons.web.api.util.HttpRequestUtil;
import com.jad.commons.web.api.util.MsgRespUtil;

public class ApiExceptionUtils {

	private static final Logger logger = LoggerFactory.getLogger(ApiExceptionUtils.class);

	@SuppressWarnings("rawtypes")
	public static void delException(HttpServletRequest request,HttpServletResponse response, Exception ex) {
		
		response.setStatus(HttpStatus.OK.value()); // 设置状态码
		response.setContentType(MediaType.APPLICATION_JSON_VALUE); // 设置ContentType
		response.setCharacterEncoding("UTF-8"); // 避免乱码
		response.setHeader("Cache-Control", "no-cache, must-revalidate");
		try {
			
			MsgReqHead reqHead = HttpRequestUtil.getReqHead(request);
			MsgResp resp = null;
			if (ex instanceof AuthenticationException) {
				
				resp = MsgRespUtil.newResp(reqHead, MsgRespCode.NO_LOGIN);
				
			} else if ((ex instanceof ApiHandlerException) && ((ApiHandlerException) ex).getCode() != null) {
				MsgRespCode code = ((ApiHandlerException) ex).getCode();
				String err = ex.getMessage() == null ? code.getMsg() : ex.getMessage();
				resp = MsgRespUtil.newResp(reqHead,code,err);
			} else {
				logger.error("请求异常:" + ex.getMessage(), ex);
				resp = MsgRespUtil.newResp(reqHead, MsgRespCode.FAIL_NOKNOW);
			}
			String msg = JsonMapper.toJsonString(resp);
			logger.debug("异常响应:" + msg);
			response.getWriter().write(msg);
		} catch (IOException e) {
			logger.error("与客户端通讯异常:" + e.getMessage(), e);
		}

	}
}



