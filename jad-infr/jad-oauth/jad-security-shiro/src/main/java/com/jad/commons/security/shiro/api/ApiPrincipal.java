package com.jad.commons.security.shiro.api;

public class ApiPrincipal {
	
	
	private String token; //
	private Object data;//此身份携带的附加数，比如用户信息，权限等
	
	public ApiPrincipal(String token) {
		this.token = token;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public Object getData() {
		return data;
	}


	public void setData(Object data) {
		this.data = data;
	}






}
