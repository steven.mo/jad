package com.jad.commons.web.api.resp;

import java.io.Serializable;

import com.jad.commons.web.api.MsgRespCode;
import com.jad.commons.web.api.req.MsgReqHead;

public class MsgResp<D extends MsgRespBody> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String trxCode;//交易码	String	否		接口编号
	private String reqTime;//请求时间	String			YYYYMMDDHHMMSS
	private String ver;//接口版本	String		1	1第一个版本
	private String deviceInfo;//	设备信息	String			客户系统标识符。
	private String dataType;//	报文类型	String		1	1表示json格式2表示xml(目前只支持json)
	private String sn;//流水号	String	否		请求流水，为方便后台日志跟踪，请尽量保持唯一。生成流水号的参考方式是：用设备串号加系统当前时间的毫秒数。
	private String token;//登录token	String			用户登录后可获取
	
	private String code	;//请求响应码	String			参见“响应码清单”一节
	private String msg	;//响应信息	String			如果业务异常，这里可查看到具体异常的原因
	
	private D data;
	
	private String signMsg;
	
	public MsgResp(){
	}
	
	public MsgResp(MsgReqHead reqHead){
		if(reqHead!=null){
			this.trxCode = reqHead.getTrxCode();
			this.reqTime=reqHead.getReqTime();
			this.ver=reqHead.getVer();
			this.deviceInfo=reqHead.getDeviceInfo();
			this.dataType=reqHead.getDataType();
			this.sn=reqHead.getSn();
			this.token=reqHead.getToken();
		}
		
	}
	
	public void setResult(MsgRespCode respCode) {
		setResult(respCode,respCode.getMsg());
	}
	public void setResult(MsgRespCode respCode,String msg) {
		this.setCode(respCode.getCode());
		if(msg!=null && !"".equals(msg)){
			this.setMsg(msg);
		}else {
			this.setMsg(respCode.getMsg());
		}
	}
	
	public void setSuccess() {
		setSuccess(MsgRespCode.SUCCESS.getMsg());
	}
	public void setSuccess(String msg) {
		this.setCode(MsgRespCode.SUCCESS.getCode());
		if(msg!=null && !"".equals(msg)){
			this.setMsg(msg);
		}else {
			this.setMsg(MsgRespCode.SUCCESS.getMsg());
		}
	}
	
	
	
	
	public D getData() {
		return data;
	}

	public void setData(D data) {
		this.data = data;
	}

	public String getSignMsg() {
		return signMsg;
	}
	public void setSignMsg(String signMsg) {
		this.signMsg = signMsg;
	}

	public String getTrxCode() {
		return trxCode;
	}

	public void setTrxCode(String trxCode) {
		this.trxCode = trxCode;
	}

	public String getReqTime() {
		return reqTime;
	}

	public void setReqTime(String reqTime) {
		this.reqTime = reqTime;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


	
	

}
