package com.jad.commons.web.api.util;

import com.jad.commons.web.api.MsgRespCode;
import com.jad.commons.web.api.req.MsgReqHead;
import com.jad.commons.web.api.resp.MsgResp;
import com.jad.commons.web.api.resp.MsgRespBody;

public class MsgRespUtil {

	
	public static <R extends MsgRespBody> MsgResp<R> newResp(MsgReqHead reqHead){
		return newResp(reqHead,null,null,null);
	}
	
	public static <R extends MsgRespBody> MsgResp<R> newResp(MsgReqHead reqHead,MsgRespBody body){
		return newResp(reqHead,body,null,null);
	}
	
	
	public static <R extends MsgRespBody> MsgResp<R> newResp(MsgReqHead reqHead,MsgRespCode respCode){
		return newResp(reqHead,null,respCode,null);
	}
	public static <R extends MsgRespBody> MsgResp<R> newResp(MsgReqHead reqHead,MsgRespCode respCode,String respInfo){
		return newResp(reqHead,null,respCode,respInfo);
	}
	
	
	public static <R extends MsgRespBody> MsgResp<R> newResp(MsgReqHead reqHead,MsgRespBody body,MsgRespCode respCode){
		return newResp(reqHead,body,respCode,null);
	}
	
	public static <R extends MsgRespBody> MsgResp<R> newResp(MsgReqHead reqHead,MsgRespBody body,MsgRespCode respCode,String respInfo){
		MsgResp resp=new MsgResp(reqHead);
		if(body!=null){
			resp.setData(body);
		}
		if(respCode!=null){
			resp.setCode(respCode.getCode());
			resp.setMsg(respInfo==null?respCode.getMsg():respInfo);
		}
		return resp;
	}
	
	public static <R extends MsgRespBody> MsgResp<R> newSuccResp(MsgReqHead reqHead){
		return newResp(reqHead,null,MsgRespCode.SUCCESS,null);
	}
	public static <R extends MsgRespBody> MsgResp<R> newSuccResp(MsgReqHead reqHead,MsgRespBody body){
		return newResp(reqHead,body,MsgRespCode.SUCCESS,null);
	}
	
	public static <R extends MsgRespBody> MsgResp<R> newSuccResp(MsgReqHead reqHead,MsgRespBody body,String respInfo){
		return newResp(reqHead,body,MsgRespCode.SUCCESS,respInfo);
	}
	public static <R extends MsgRespBody> MsgResp<R> newSuccResp(MsgReqHead reqHead,String respInfo){
		return newResp(reqHead,null,MsgRespCode.SUCCESS,respInfo);
	}
	
	
	
	
	
	
	
	
	
}
