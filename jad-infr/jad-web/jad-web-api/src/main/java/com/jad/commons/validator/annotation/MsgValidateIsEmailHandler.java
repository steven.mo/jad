package com.jad.commons.validator.annotation;

import java.lang.reflect.Field;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <li>电子邮箱属性校验</li>
 * 
 */
public class MsgValidateIsEmailHandler implements MsgValidateHandler {

	private final static Logger logger = LoggerFactory.getLogger(MsgValidateIsEmailHandler.class);
    
	private void check(MsgValidable filter, Field field) {
		
		MsgValidateIsEmail a=field.getAnnotation(MsgValidateIsEmail.class);
		
		Object dest = null;
		try {
			dest = GetFiledValue.getField(filter, field.getName());
			
		} catch (Exception ex) {
			logger.error("校检[" + field.getName() + "] 的格式时出现异常,"+ex.getMessage(),ex);
			throw new MsgAnnValidateException(ex.getMessage(), ex);
		}
		
		if(dest == null || "".equals(dest)){
			if(!a.canBlank()){
				String error="[" + field.getName() + "]不能为空";
				logger.error(error);
				throw new MsgAnnValidateException(error);
			}
		}else if(dest instanceof String){
			
			String email=dest.toString().trim();
			
			if(!isEmail(email) ){
				String error="[" + field.getName() + "]的值["+email+"]必须为邮箱格式";
				logger.error(error);
				throw new MsgAnnValidateException(error);
			}
			
		}else{
			logger.error("校检[" + field.getName() + "] 的格式时出现异常");
			throw new MsgAnnValidateException("该注解只能用来校验类型为 String 的属性");
		}
		


	}

	public static boolean isEmail(String email){  
        if (null==email || "".equals(email)) return false;    
        Pattern p =  Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");//复杂匹配  
        Matcher m = p.matcher(email);  
        return m.matches();  
     }
	
	public void validate(MsgValidable validatedObj, Field field) {
		if (field.isAnnotationPresent(MsgValidateIsEmail.class)) {
			check(validatedObj, field);
		}

	}

}
