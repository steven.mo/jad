package com.jad.commons.validator;

/**
 * 报文校验异常
 * @author hechuan
 *
 */
public class MsgValidateException extends RuntimeException {

	public MsgValidateException() {
	}

	public MsgValidateException(String message) {
		super(message);
	}

	public MsgValidateException(Throwable cause) {
		super(cause);
	}

	public MsgValidateException(String message, Throwable cause) {
		super(message, cause);
	}

}
