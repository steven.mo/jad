package com.jad.commons.validator.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <li>校验接口, 这是一个单例，使用时直接调用validate()即可</li>
 * 
 */
public class MsgAnnotationValidator {

	private final static Logger logger = LoggerFactory.getLogger(MsgAnnotationValidator.class);
	
	private static MsgAnnotationValidator validator = new MsgAnnotationValidator();

	public final static String SUFFIX = "Handler";

	public final static String PREFIX = "MsgValidate";

	private static Map<String,MsgValidateHandler>HANDLER=new HashMap<String,MsgValidateHandler>();
	
	public static MsgAnnotationValidator getInstance() {
		return validator;
	}

	private MsgAnnotationValidator() {
	}

	private List<Annotation> getValidateAnnotations(Field field) {
		List<Annotation> annotations = new ArrayList<Annotation>();
		Annotation[] annos = field.getAnnotations();
		for (Annotation elem : annos) {
			if (elem.annotationType().getSimpleName().startsWith(PREFIX)) {
				annotations.add(elem);
			}
		}

		return annotations;
	}

	public void validate(MsgValidable validatedObj) throws MsgAnnValidateException {
		if (null == validatedObj) {
			return;
		}
		Class currentClass = validatedObj.getClass();
		while (currentClass != null) {// 递归校验父类
			Field[] fields = currentClass.getDeclaredFields();
			for (Field elem : fields) {
				validateField(validatedObj, elem);
			}
			Class superClass = currentClass.getSuperclass();
			currentClass = MsgValidable.class.isAssignableFrom(superClass) ? superClass : null;
		}

	}

	
	
	@SuppressWarnings("rawtypes")
	private void validateField(MsgValidable validatedObj, Field field) throws MsgAnnValidateException {
//		add 2170720
		if(MsgValidable.class.isAssignableFrom(field.getType())){
			MsgValidable fieldVal =null;
			try {
				fieldVal =(MsgValidable)GetFiledValue.getField(validatedObj, field.getName());
			}  catch (Exception e) {
				logger.error("无法获取类"+validatedObj.getClass().getName()+"的字段"+field.getName()+"的值,"+e.getMessage(),e);
			}
			if(fieldVal!=null){
				validate(fieldVal);
			}
		}
		else if(field.getType().isArray()){
			Object[]fieldVals=null;
			try {
				fieldVals=(Object[])GetFiledValue.getField(validatedObj, field.getName());
			}  catch (Exception e) {
				logger.error("无法获取类"+validatedObj.getClass().getName()+"的字段"+field.getName()+"的值,"+e.getMessage(),e);
			}
			if(fieldVals!=null && fieldVals.length>0){
				for(Object val:fieldVals){
					if(val!=null && (val instanceof MsgValidable)){
						validate((MsgValidable)val);
					}
				}
			}
		}
		else if(Collection.class.isAssignableFrom(field.getType())){
			Collection fieldVals=null;
			try {
				fieldVals=(Collection)GetFiledValue.getField(validatedObj, field.getName());
			}  catch (Exception e) {
				logger.error("无法获取类"+validatedObj.getClass().getName()+"的字段"+field.getName()+"的值,"+e.getMessage(),e);
			}
			if(fieldVals!=null && fieldVals.size()>0){
				for(Object val:fieldVals){
					if(val!=null && (val instanceof MsgValidable)){
						validate((MsgValidable)val);
					}
				}
			}
		}
		else if(Map.class.isAssignableFrom(field.getType())){
			Map fildValMap=null;
			try {
				fildValMap=(Map)GetFiledValue.getField(validatedObj, field.getName());
			}  catch (Exception e) {
				logger.error("无法获取类"+validatedObj.getClass().getName()+"的字段"+field.getName()+"的值,"+e.getMessage(),e);
			}
			if(fildValMap!=null && fildValMap.size()>0){
				Collection fieldVals=fildValMap.values();
				for(Object val:fieldVals){
					if(val!=null && (val instanceof MsgValidable)){
						validate((MsgValidable)val);
					}
				}
			}
		}
		
		List<Annotation> annotations = getValidateAnnotations(field);
		if (annotations != null && annotations.size() > 0) {
			for (Annotation annotation : annotations) {
				String annotationName = annotation.annotationType().getName();
				
				String className = annotationName + SUFFIX;
				MsgValidateHandler handler = HANDLER.get(className);
				try {
					if(handler==null){
						handler = (MsgValidateHandler) Class.forName(className).newInstance();
						HANDLER.put(className, handler);
					}
					
				} catch (Exception ex) {
					throw new MsgAnnValidateException("校验失败，无法获得校检类[" + className + "]处理器：" + ex.getMessage(), ex);
				}
				handler.validate(validatedObj, field);
			}
		}
	}
	
	

}
