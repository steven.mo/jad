package com.jad.web.mvc.dynamic.exception;

public class DynamicReqException extends RuntimeException{

	public DynamicReqException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DynamicReqException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DynamicReqException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DynamicReqException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DynamicReqException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}


}
