package com.jad.web.api.resolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.jad.commons.security.shiro.api.ApiExceptionUtils;

public class ApiHandlerExceptionResolver extends SimpleMappingExceptionResolver{

	private static final Logger logger = LoggerFactory.getLogger(ApiHandlerExceptionResolver.class);
	
	@Override
	public ModelAndView resolveException(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex) {
		
		if(request.getAttribute("JSON_REQUEST_BODY")!=null){
			
			ApiExceptionUtils.delException(request,response,ex);
			
			ModelAndView mv = new ModelAndView();
	       
	        return mv;
	        
		}else{
			return super.resolveException(request, response, handler, ex);
			
		}
		
	}
	
	


}
