package com.jad.cache.memcache;

import com.alisoft.xplatform.asf.cache.IMemcachedCache;
import com.alisoft.xplatform.asf.cache.memcached.MemcachedCacheManager;
import com.jad.cache.common.CacheException;


/**
 * memcache实现的CacheManager
 *
 */
public class JadMemcachedCacheManager extends MemcachedCacheManager {

	
	/**
	 * 是否复用主缓存
	 * 
	 * 在autoCreateCache属性为true时，可自动创建缺省配置的cache
	 * 但是在此属性为true的情况下，不再自动创建一个新的cache,而是直接复用 masterCacheName 所指定的 cache
	 * 
	 * (目前只支持复用，待完善)
	 * 
	 */
	private boolean multiplexMasterCache = true;
	
	/**
	 * 主缓存名称
	 * 
	 * memcache.xml配置文件中因该至少有一个client标签
	 * 如果配置文件中有多个client标签时，需要指定一个主要的缓存,用于是在autoCreateCache属性为true时可以自动创建缺省配置的cache实例
	 * 
	 */
	private String masterCacheName ;
	
	public IMemcachedCache getCache(String name){
		IMemcachedCache cache = super.getCache(name);
		
//		if(cache == null && this.multiplexMasterCache){//复用主缓存
		if(cache == null ){//暂时全部复用缓存，待完善
			cache = super.getCache(masterCacheName);
		}
		
		if(cache ==null ){
//			TODO 在非复用主缓存的情况下，这里应该自主创建一个cache。暂时还没有完成，先抛异常
			throw new CacheException("没有获取到名称为:"+name+"缓存");
		}
		return cache;
	}


	public boolean isMultiplexMasterCache() {
		return multiplexMasterCache;
	}


	public void setMultiplexMasterCache(boolean multiplexMasterCache) {
		this.multiplexMasterCache = multiplexMasterCache;
	}


	public String getMasterCacheName() {
		return masterCacheName;
	}


	public void setMasterCacheName(String masterCacheName) {
		this.masterCacheName = masterCacheName;
	}


	
	
	

}
