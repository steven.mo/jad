package com.jad.cache.common.api;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.util.Assert;

import com.jad.cache.common.CacheException;
import com.jad.cache.common.ManageredCache;
import com.jad.cache.common.MasterCacheManager;


/**
 * 缓存工具类
 * @author Administrator
 *
 */
public class CacheHelper {
	
	/**
	 * 主缓存缓存管理器
	 */
	private static MasterCacheManager MASTER_CACHE_MANAGER;
	
	
	/**
	 * 是否初始化
	 */
	private static volatile boolean INITIATED = false ;
	
	/**
	 * 当前类的实例集合
	 */
	private static final ConcurrentMap<String,CacheHelper> INSTANCE_MAP = new ConcurrentHashMap<String,CacheHelper>();
	
	/**
	 * 缓存名称
	 */
	private String cacheName;
	
	/**
	 * 私人化构造函数
	 */
	private CacheHelper(String cacheName){
		this.cacheName = cacheName;
	}
	
	/**
	 * 获取当前类的实例
	 * @return
	 */
	public static CacheHelper getInstance(String cacheName){
		Assert.notNull(cacheName);
		assertInited();
		CacheHelper helper = INSTANCE_MAP.get(cacheName);
		if(helper==null){
			helper = new CacheHelper(cacheName);
			CacheHelper old = INSTANCE_MAP.putIfAbsent(cacheName, helper);
			if(old != null){
				helper = old;
			}
		}
		return helper;
	}
	
	
	/**
	 * 从缓存中获取数据
	 * @param key
	 * @return
	 */
	public Object get(String key){
		return getCache().getVal(key);
	}
	
	/**
	 * 把数据保存到缓存中
	 * @param key
	 * @param value
	 */
	public void put(String key,Serializable value){
		this.getCache().put(key, value);
	}
	
	public void put(String key,Serializable value,int activityTime){
		this.getCache().put(key, value,activityTime);
	}
	

	/**
	 * 从缓存中删除数据
	 * @param key
	 */
	public void evict(String key){
		getCache().evict(key);
	}
	
	/**
	 * 清空数据
	 */
	public void clear(){
		getCache().clear();
	}
	
	
	/**
	 * 初始化
	 * @param masterCacheManager
	 */
	public static void init(MasterCacheManager masterCacheManager){
		MASTER_CACHE_MANAGER = masterCacheManager;
		INITIATED = true;
	}
	
	/**
	 * 获得cache
	 * @return
	 */
	private ManageredCache getCache(){
		return (ManageredCache)MASTER_CACHE_MANAGER.getCache(cacheName);
	}
	
	/**
	 * 确保已经初始化
	 */
	private static void assertInited(){
		if(!INITIATED){
			throw new CacheException("缓存还没有初始化");
		}
	}
	
	
	
	
	
}
