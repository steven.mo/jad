package com.jad.cache.common.impl;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;

import com.jad.cache.common.AbstractCacheClient;
import com.jad.cache.common.AbstractCacheClientManager;
import com.jad.cache.common.JadCacheManager;

/**
 * 简单的本地缓存客户端
 *
 */
public class SimpleLocalCacheClient extends AbstractCacheClient   {
	
	@Override
	protected void registryCacheManager(BeanDefinitionRegistry registry) {
		
		String cacheManagerBeanName=this.getClientName()+"_"+AbstractCacheClientManager.CACHE_MANAGER_BEAN_NAME;
		
		if (!registry.containsBeanDefinition(cacheManagerBeanName)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(SimpleLocalCacheManager.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			beanDefinition.getPropertyValues().add("cacheClient", this);
			
			
			registry.registerBeanDefinition(cacheManagerBeanName, beanDefinition);
			
			JadCacheManager manager = (JadCacheManager)applicationContext.getBean(cacheManagerBeanName);
			this.setCacheManager(manager);
			registryToMasterCacheManager(manager);//注册到主管理器,单客户端只有一个管理器，直接为默认
			
		}
		
	}

	
	




}
