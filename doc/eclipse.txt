eclipse下载地址
下载列表页
https://www.eclipse.org/downloads/eclipse-packages/

Eclipse Juno (eclipse 4.2) for javaee (jad插件不支持)
https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/junosr2

Eclipse Kepler (eclipse 4.3)  for javaee 
https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/keplersr2

Eclipse Luna (eclipse 4.4)  for javaee (推荐用这个)
https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/lunasr2

Eclipse Mars (eclipse 4.5) for javaee 
https://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/mars2

Eclipse Neon (eclipse 4.6) for javaee (需要jdk1.8，暂没测试)
http://www.eclipse.org/downloads/packages/eclipse-ide-java-ee-developers/neon3

eclipse源码git下载地址
http://git.eclipse.org/c/platform/
eclipse源码下载地址
http://download.eclipse.org/eclipse/downloads/





