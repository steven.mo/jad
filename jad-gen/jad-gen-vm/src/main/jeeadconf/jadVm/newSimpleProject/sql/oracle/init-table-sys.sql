------------------------------------------------
-- Export file for user CASHOUT               --
-- Created by hechuan on 2017-11-20, 15:33:19 --
------------------------------------------------

spool init-table.log

prompt
prompt Creating table SYS_AREA
prompt =======================
prompt
create table SYS_AREA
(
  ID          VARCHAR2(64) not null,
  PARENT_ID   VARCHAR2(64) not null,
  PARENT_IDS  VARCHAR2(2000) not null,
  CODE        VARCHAR2(100),
  NAME        VARCHAR2(100) not null,
  TYPE        CHAR(1),
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  UPDATE_BY   VARCHAR2(64),
  UPDATE_DATE TIMESTAMP(6),
  REMARKS     VARCHAR2(255),
  DEL_FLAG    CHAR(1) default '0' not null,
  SORT        NUMBER
)
;
comment on table SYS_AREA
  is '区域表';
comment on column SYS_AREA.ID
  is '编号';
comment on column SYS_AREA.PARENT_ID
  is '父级编号';
comment on column SYS_AREA.PARENT_IDS
  is '所有父级编号';
comment on column SYS_AREA.CODE
  is '区域编码';
comment on column SYS_AREA.NAME
  is '区域名称';
comment on column SYS_AREA.TYPE
  is '区域类型';
comment on column SYS_AREA.CREATE_BY
  is '创建者';
comment on column SYS_AREA.CREATE_DATE
  is '创建时间';
comment on column SYS_AREA.UPDATE_BY
  is '更新者';
comment on column SYS_AREA.UPDATE_DATE
  is '更新时间';
comment on column SYS_AREA.REMARKS
  is '备注信息';
comment on column SYS_AREA.DEL_FLAG
  is '删除标记';
comment on column SYS_AREA.SORT
  is '排序';
alter table SYS_AREA
  add primary key (ID);
create index SYS_AREA_DEL_FLAG on SYS_AREA (DEL_FLAG);
create index SYS_AREA_PARENT_ID on SYS_AREA (PARENT_ID);
create index SYS_AREA_PARENT_IDS on SYS_AREA (PARENT_IDS);

prompt
prompt Creating table SYS_DICT
prompt =======================
prompt
create table SYS_DICT
(
  ID          VARCHAR2(64) not null,
  LABEL       VARCHAR2(100) not null,
  VALUE       VARCHAR2(100) not null,
  TYPE        VARCHAR2(100) not null,
  DESCRIPTION VARCHAR2(100) not null,
  SORT        NUMBER(10) not null,
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  UPDATE_BY   VARCHAR2(64),
  UPDATE_DATE TIMESTAMP(6),
  REMARKS     VARCHAR2(255),
  DEL_FLAG    CHAR(1) default '0' not null,
  PARENT_ID   VARCHAR2(64)
)
;
comment on table SYS_DICT
  is '字典表';
comment on column SYS_DICT.ID
  is '编号';
comment on column SYS_DICT.LABEL
  is '标签名';
comment on column SYS_DICT.VALUE
  is '数据值';
comment on column SYS_DICT.TYPE
  is '类型';
comment on column SYS_DICT.DESCRIPTION
  is '描述';
comment on column SYS_DICT.SORT
  is '排序（升序）';
comment on column SYS_DICT.CREATE_BY
  is '创建者';
comment on column SYS_DICT.CREATE_DATE
  is '创建时间';
comment on column SYS_DICT.UPDATE_BY
  is '更新者';
comment on column SYS_DICT.UPDATE_DATE
  is '更新时间';
comment on column SYS_DICT.REMARKS
  is '备注信息';
comment on column SYS_DICT.DEL_FLAG
  is '删除标记';
comment on column SYS_DICT.PARENT_ID
  is '父Id';
alter table SYS_DICT
  add primary key (ID);
create index SYS_DICT_DEL_FLAG on SYS_DICT (DEL_FLAG);
create index SYS_DICT_LABEL on SYS_DICT (LABEL);
create index SYS_DICT_VALUE on SYS_DICT (VALUE);

prompt
prompt Creating table SYS_LOG
prompt ======================
prompt
create table SYS_LOG
(
  ID          VARCHAR2(64) not null,
  TYPE        CHAR(1) default '1',
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  REMOTE_ADDR VARCHAR2(255),
  USER_AGENT  VARCHAR2(255),
  REQUEST_URI VARCHAR2(255),
  METHOD      VARCHAR2(5),
  PARAMS      VARCHAR2(3500),
  EXCEPTION   VARCHAR2(3500)
)
;
comment on table SYS_LOG
  is '日志表';
comment on column SYS_LOG.ID
  is '编号';
comment on column SYS_LOG.TYPE
  is '日志类型';
comment on column SYS_LOG.CREATE_BY
  is '创建者';
comment on column SYS_LOG.CREATE_DATE
  is '创建时间';
comment on column SYS_LOG.REMOTE_ADDR
  is '操作IP地址';
comment on column SYS_LOG.USER_AGENT
  is '用户代理';
comment on column SYS_LOG.REQUEST_URI
  is '请求URI';
comment on column SYS_LOG.METHOD
  is '操作方式';
comment on column SYS_LOG.PARAMS
  is '操作参数';
comment on column SYS_LOG.EXCEPTION
  is '异常信息';
alter table SYS_LOG
  add primary key (ID);
create index SYS_LOG_CREATE_BY on SYS_LOG (CREATE_BY);
create index SYS_LOG_CREATE_DATE on SYS_LOG (CREATE_DATE);
create index SYS_LOG_REQUEST_URI on SYS_LOG (REQUEST_URI);
create index SYS_LOG_TYPE on SYS_LOG (TYPE);

prompt
prompt Creating table SYS_MDICT
prompt ========================
prompt
create table SYS_MDICT
(
  ID          VARCHAR2(64) not null,
  PARENT_ID   VARCHAR2(64) not null,
  PARENT_IDS  VARCHAR2(2000) not null,
  NAME        VARCHAR2(100) not null,
  DESCRIPTION VARCHAR2(100),
  SORT        NUMBER(10),
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  UPDATE_BY   VARCHAR2(64),
  UPDATE_DATE TIMESTAMP(6),
  REMARKS     VARCHAR2(255),
  DEL_FLAG    CHAR(1) default '0' not null
)
;
comment on table SYS_MDICT
  is '多级字典表';
comment on column SYS_MDICT.ID
  is '编号';
comment on column SYS_MDICT.PARENT_ID
  is '父级编号';
comment on column SYS_MDICT.PARENT_IDS
  is '所有父级编号';
comment on column SYS_MDICT.NAME
  is '角色名称';
comment on column SYS_MDICT.DESCRIPTION
  is '描述';
comment on column SYS_MDICT.SORT
  is '排序（升序）';
comment on column SYS_MDICT.CREATE_BY
  is '创建者';
comment on column SYS_MDICT.CREATE_DATE
  is '创建时间';
comment on column SYS_MDICT.UPDATE_BY
  is '更新者';
comment on column SYS_MDICT.UPDATE_DATE
  is '更新时间';
comment on column SYS_MDICT.REMARKS
  is '备注信息';
comment on column SYS_MDICT.DEL_FLAG
  is '删除标记';
alter table SYS_MDICT
  add primary key (ID);
create index SYS_MDICT_DEL_FLAG on SYS_MDICT (DEL_FLAG);
create index SYS_MDICT_PARENT_ID on SYS_MDICT (PARENT_ID);
create index SYS_MDICT_PARENT_IDS on SYS_MDICT (PARENT_IDS);

prompt
prompt Creating table SYS_MENU
prompt =======================
prompt
create table SYS_MENU
(
  ID          VARCHAR2(64) not null,
  PARENT_ID   VARCHAR2(64) not null,
  PARENT_IDS  VARCHAR2(2000) not null,
  NAME        VARCHAR2(100) not null,
  HREF        VARCHAR2(255),
  TARGET      VARCHAR2(20),
  ICON        VARCHAR2(100),
  SORT        NUMBER(10) not null,
  IS_SHOW     CHAR(1) not null,
  IS_ACTIVITI CHAR(1),
  PERMISSION  VARCHAR2(200),
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  UPDATE_BY   VARCHAR2(64),
  UPDATE_DATE TIMESTAMP(6),
  REMARKS     VARCHAR2(255),
  DEL_FLAG    CHAR(1) default '0' not null
)
;
comment on table SYS_MENU
  is '菜单表';
comment on column SYS_MENU.ID
  is '编号';
comment on column SYS_MENU.PARENT_ID
  is '父级编号';
comment on column SYS_MENU.PARENT_IDS
  is '所有父级编号';
comment on column SYS_MENU.NAME
  is '菜单名称';
comment on column SYS_MENU.HREF
  is '链接';
comment on column SYS_MENU.TARGET
  is '目标';
comment on column SYS_MENU.ICON
  is '图标';
comment on column SYS_MENU.SORT
  is '排序（升序）';
comment on column SYS_MENU.IS_SHOW
  is '是否在菜单中显示';
comment on column SYS_MENU.IS_ACTIVITI
  is '是否同步工作流';
comment on column SYS_MENU.PERMISSION
  is '权限标识';
comment on column SYS_MENU.CREATE_BY
  is '创建者';
comment on column SYS_MENU.CREATE_DATE
  is '创建时间';
comment on column SYS_MENU.UPDATE_BY
  is '更新者';
comment on column SYS_MENU.UPDATE_DATE
  is '更新时间';
comment on column SYS_MENU.REMARKS
  is '备注信息';
comment on column SYS_MENU.DEL_FLAG
  is '删除标记';
alter table SYS_MENU
  add primary key (ID);
create index SYS_MENU_DEL_FLAG on SYS_MENU (DEL_FLAG);
create index SYS_MENU_PARENT_ID on SYS_MENU (PARENT_ID);
create index SYS_MENU_PARENT_IDS on SYS_MENU (PARENT_IDS);

prompt
prompt Creating table SYS_OFFICE
prompt =========================
prompt
create table SYS_OFFICE
(
  ID             VARCHAR2(64) not null,
  PARENT_ID      VARCHAR2(64) not null,
  PARENT_IDS     VARCHAR2(2000) not null,
  AREA_ID        VARCHAR2(64) not null,
  CODE           VARCHAR2(100),
  NAME           VARCHAR2(100) not null,
  TYPE           CHAR(1) not null,
  GRADE          CHAR(1) not null,
  ADDRESS        VARCHAR2(255),
  ZIP_CODE       VARCHAR2(100),
  MASTER         VARCHAR2(100),
  PHONE          VARCHAR2(200),
  FAX            VARCHAR2(200),
  EMAIL          VARCHAR2(200),
  CREATE_BY      VARCHAR2(64),
  CREATE_DATE    TIMESTAMP(6),
  UPDATE_BY      VARCHAR2(64),
  UPDATE_DATE    TIMESTAMP(6),
  REMARKS        VARCHAR2(255),
  DEL_FLAG       CHAR(1) default '0' not null,
  USEABLE        VARCHAR2(50),
  SORT           NUMBER(10),
  PRIMARY_PERSON VARCHAR2(100),
  DEPUTY_PERSON  VARCHAR2(100)
)
;
comment on table SYS_OFFICE
  is '机构表';
comment on column SYS_OFFICE.ID
  is '编号';
comment on column SYS_OFFICE.PARENT_ID
  is '父级编号';
comment on column SYS_OFFICE.PARENT_IDS
  is '所有父级编号';
comment on column SYS_OFFICE.AREA_ID
  is '归属区域';
comment on column SYS_OFFICE.CODE
  is '区域编码';
comment on column SYS_OFFICE.NAME
  is '机构名称';
comment on column SYS_OFFICE.TYPE
  is '机构类型';
comment on column SYS_OFFICE.GRADE
  is '机构等级';
comment on column SYS_OFFICE.ADDRESS
  is '联系地址';
comment on column SYS_OFFICE.ZIP_CODE
  is '邮政编码';
comment on column SYS_OFFICE.MASTER
  is '负责人';
comment on column SYS_OFFICE.PHONE
  is '电话';
comment on column SYS_OFFICE.FAX
  is '传真';
comment on column SYS_OFFICE.EMAIL
  is '邮箱';
comment on column SYS_OFFICE.CREATE_BY
  is '创建者';
comment on column SYS_OFFICE.CREATE_DATE
  is '创建时间';
comment on column SYS_OFFICE.UPDATE_BY
  is '更新者';
comment on column SYS_OFFICE.UPDATE_DATE
  is '更新时间';
comment on column SYS_OFFICE.REMARKS
  is '备注信息';
comment on column SYS_OFFICE.DEL_FLAG
  is '删除标记';
comment on column SYS_OFFICE.USEABLE
  is '是否可用';
comment on column SYS_OFFICE.SORT
  is '排序（升序）';
comment on column SYS_OFFICE.PRIMARY_PERSON
  is '主负责人';
comment on column SYS_OFFICE.DEPUTY_PERSON
  is '副负责人';
alter table SYS_OFFICE
  add primary key (ID);
create index SYS_OFFICE_DEL_FLAG on SYS_OFFICE (DEL_FLAG);
create index SYS_OFFICE_PARENT_ID on SYS_OFFICE (PARENT_ID);
create index SYS_OFFICE_PARENT_IDS on SYS_OFFICE (PARENT_IDS);

prompt
prompt Creating table SYS_ROLE
prompt =======================
prompt
create table SYS_ROLE
(
  ID          VARCHAR2(64) not null,
  OFFICE_ID   VARCHAR2(64),
  NAME        VARCHAR2(100) not null,
  DATA_SCOPE  CHAR(1),
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  UPDATE_BY   VARCHAR2(64),
  UPDATE_DATE TIMESTAMP(6),
  REMARKS     VARCHAR2(255),
  DEL_FLAG    CHAR(1) default '0' not null,
  USEABLE     VARCHAR2(1),
  IS_SYS      VARCHAR2(1),
  ROLE_TYPE   VARCHAR2(64),
  ENNAME      VARCHAR2(100)
)
;
comment on table SYS_ROLE
  is '角色表';
comment on column SYS_ROLE.ID
  is '编号';
comment on column SYS_ROLE.OFFICE_ID
  is '归属机构';
comment on column SYS_ROLE.NAME
  is '角色名称';
comment on column SYS_ROLE.DATA_SCOPE
  is '数据范围';
comment on column SYS_ROLE.CREATE_BY
  is '创建者';
comment on column SYS_ROLE.CREATE_DATE
  is '创建时间';
comment on column SYS_ROLE.UPDATE_BY
  is '更新者';
comment on column SYS_ROLE.UPDATE_DATE
  is '更新时间';
comment on column SYS_ROLE.REMARKS
  is '备注信息';
comment on column SYS_ROLE.DEL_FLAG
  is '删除标记';
comment on column SYS_ROLE.USEABLE
  is '是否是可用';
comment on column SYS_ROLE.IS_SYS
  is '是否是系统数据';
comment on column SYS_ROLE.ROLE_TYPE
  is '权限类型';
comment on column SYS_ROLE.ENNAME
  is '英文名称';
alter table SYS_ROLE
  add primary key (ID);
create index SYS_ROLE_DEL_FLAG on SYS_ROLE (DEL_FLAG);

prompt
prompt Creating table SYS_ROLE_MENU
prompt ============================
prompt
create table SYS_ROLE_MENU
(
  ROLE_ID VARCHAR2(64) not null,
  MENU_ID VARCHAR2(64) not null
)
;
comment on table SYS_ROLE_MENU
  is '角色-菜单';
comment on column SYS_ROLE_MENU.ROLE_ID
  is '角色编号';
comment on column SYS_ROLE_MENU.MENU_ID
  is '菜单编号';
alter table SYS_ROLE_MENU
  add primary key (ROLE_ID, MENU_ID);

prompt
prompt Creating table SYS_ROLE_OFFICE
prompt ==============================
prompt
create table SYS_ROLE_OFFICE
(
  ROLE_ID   VARCHAR2(64) not null,
  OFFICE_ID VARCHAR2(64) not null
)
;
comment on table SYS_ROLE_OFFICE
  is '角色-机构';
comment on column SYS_ROLE_OFFICE.ROLE_ID
  is '角色编号';
comment on column SYS_ROLE_OFFICE.OFFICE_ID
  is '机构编号';
alter table SYS_ROLE_OFFICE
  add primary key (ROLE_ID, OFFICE_ID);

prompt
prompt Creating table SYS_USER
prompt =======================
prompt
create table SYS_USER
(
  ID          VARCHAR2(64) not null,
  COMPANY_ID  VARCHAR2(64) not null,
  OFFICE_ID   VARCHAR2(64) not null,
  LOGIN_NAME  VARCHAR2(100) not null,
  PASSWORD    VARCHAR2(100) not null,
  NO          VARCHAR2(100),
  NAME        VARCHAR2(100) not null,
  EMAIL       VARCHAR2(200),
  PHONE       VARCHAR2(200),
  MOBILE      VARCHAR2(200),
  USER_TYPE   CHAR(1),
  LOGIN_IP    VARCHAR2(100),
  LOGIN_DATE  TIMESTAMP(6),
  CREATE_BY   VARCHAR2(64),
  CREATE_DATE TIMESTAMP(6),
  UPDATE_BY   VARCHAR2(64),
  UPDATE_DATE TIMESTAMP(6),
  REMARKS     VARCHAR2(255),
  DEL_FLAG    CHAR(1) default '0' not null,
  PHOTO       VARCHAR2(255),
  LOGIN_FLAG  VARCHAR2(64)
)
;
comment on table SYS_USER
  is '用户表';
comment on column SYS_USER.ID
  is '编号';
comment on column SYS_USER.COMPANY_ID
  is '归属公司';
comment on column SYS_USER.OFFICE_ID
  is '归属部门';
comment on column SYS_USER.LOGIN_NAME
  is '登录名';
comment on column SYS_USER.PASSWORD
  is '密码';
comment on column SYS_USER.NO
  is '工号';
comment on column SYS_USER.NAME
  is '姓名';
comment on column SYS_USER.EMAIL
  is '邮箱';
comment on column SYS_USER.PHONE
  is '电话';
comment on column SYS_USER.MOBILE
  is '手机';
comment on column SYS_USER.USER_TYPE
  is '用户类型';
comment on column SYS_USER.LOGIN_IP
  is '最后登陆IP';
comment on column SYS_USER.LOGIN_DATE
  is '最后登陆时间';
comment on column SYS_USER.CREATE_BY
  is '创建者';
comment on column SYS_USER.CREATE_DATE
  is '创建时间';
comment on column SYS_USER.UPDATE_BY
  is '更新者';
comment on column SYS_USER.UPDATE_DATE
  is '更新时间';
comment on column SYS_USER.REMARKS
  is '备注信息';
comment on column SYS_USER.DEL_FLAG
  is '删除标记';
comment on column SYS_USER.PHOTO
  is '头像';
comment on column SYS_USER.LOGIN_FLAG
  is '是否允许登陆';
alter table SYS_USER
  add primary key (ID);
create index SYS_USER_COMPANY_ID on SYS_USER (COMPANY_ID);
create index SYS_USER_DEL_FLAG on SYS_USER (DEL_FLAG);
create index SYS_USER_LOGIN_NAME on SYS_USER (LOGIN_NAME);
create index SYS_USER_OFFICE_ID on SYS_USER (OFFICE_ID);
create index SYS_USER_UPDATE_DATE on SYS_USER (UPDATE_DATE);

prompt
prompt Creating table SYS_USER_ROLE
prompt ============================
prompt
create table SYS_USER_ROLE
(
  USER_ID VARCHAR2(64) not null,
  ROLE_ID VARCHAR2(64) not null
)
;
comment on table SYS_USER_ROLE
  is '用户-角色';
comment on column SYS_USER_ROLE.USER_ID
  is '用户编号';
comment on column SYS_USER_ROLE.ROLE_ID
  is '角色编号';
alter table SYS_USER_ROLE
  add primary key (USER_ID, ROLE_ID);


spool off
