prompt PL/SQL Developer import file
prompt Created on 星期一, 十一月 20, 2017 by hechuan
set feedback off
set define off
prompt Loading SYS_AREA...
insert into SYS_AREA (ID, PARENT_ID, PARENT_IDS, CODE, NAME, TYPE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, SORT)
values ('1', '0', '0,', '100000', '中国', '1', '1', null, '1', null, null, '0', 10);
insert into SYS_AREA (ID, PARENT_ID, PARENT_IDS, CODE, NAME, TYPE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, SORT)
values ('3', '2', '0,1,2,', '110001', '深圳市', '3', '1', null, '1', null, null, '0', 30);
insert into SYS_AREA (ID, PARENT_ID, PARENT_IDS, CODE, NAME, TYPE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, SORT)
values ('2', '1', '0,1,', '110000', '广东省', '2', '1', null, '1', null, null, '0', 20);
commit;
prompt 3 records loaded
prompt Loading SYS_DICT...
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('1', '正常', '0', 'del_flag', '删除标记', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('2', '删除', '1', 'del_flag', '删除标记', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('3', '显示', '1', 'show_hide', '显示/隐藏', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('4', '隐藏', '0', 'show_hide', '显示/隐藏', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('5', '是', '1', 'yes_no', '是/否', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('6', '否', '0', 'yes_no', '是/否', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('7', '红色', 'red', 'color', '颜色值', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('8', '绿色', 'green', 'color', '颜色值', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('9', '蓝色', 'blue', 'color', '颜色值', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('10', '黄色', 'yellow', 'color', '颜色值', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('11', '橙色', 'orange', 'color', '颜色值', 50, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('12', '默认主题', 'default', 'theme', '主题方案', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('13', '天蓝主题', 'cerulean', 'theme', '主题方案', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('14', '橙色主题', 'readable', 'theme', '主题方案', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('15', '红色主题', 'united', 'theme', '主题方案', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('16', 'Flat主题', 'flat', 'theme', '主题方案', 60, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('17', '国家', '1', 'sys_area_type', '区域类型', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('18', '省份、直辖市', '2', 'sys_area_type', '区域类型', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('19', '地市', '3', 'sys_area_type', '区域类型', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('20', '区县', '4', 'sys_area_type', '区域类型', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('21', '公司', '1', 'sys_office_type', '机构类型', 60, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('22', '部门', '2', 'sys_office_type', '机构类型', 70, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('23', '一级', '1', 'sys_office_grade', '机构等级', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('24', '二级', '2', 'sys_office_grade', '机构等级', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('25', '三级', '3', 'sys_office_grade', '机构等级', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('26', '四级', '4', 'sys_office_grade', '机构等级', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('27', '所有数据', '1', 'sys_data_scope', '数据范围', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('28', '所在公司及以下数据', '2', 'sys_data_scope', '数据范围', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('29', '所在公司数据', '3', 'sys_data_scope', '数据范围', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('30', '所在部门及以下数据', '4', 'sys_data_scope', '数据范围', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('31', '所在部门数据', '5', 'sys_data_scope', '数据范围', 50, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('32', '仅本人数据', '8', 'sys_data_scope', '数据范围', 90, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('33', '按明细设置', '9', 'sys_data_scope', '数据范围', 100, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('34', '系统管理', '1', 'sys_user_type', '用户类型', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('35', '部门经理', '2', 'sys_user_type', '用户类型', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('36', '普通用户', '3', 'sys_user_type', '用户类型', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('37', '基础主题', 'basic', 'cms_theme', '站点主题', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('38', '蓝色主题', 'blue', 'cms_theme', '站点主题', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '1', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('39', '红色主题', 'red', 'cms_theme', '站点主题', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '1', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('40', '文章模型', 'article', 'cms_module', '栏目模型', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('41', '图片模型', 'picture', 'cms_module', '栏目模型', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '1', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('42', '下载模型', 'download', 'cms_module', '栏目模型', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '1', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('43', '链接模型', 'link', 'cms_module', '栏目模型', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('44', '专题模型', 'special', 'cms_module', '栏目模型', 50, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '1', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('45', '默认展现方式', '0', 'cms_show_modes', '展现方式', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('46', '首栏目内容列表', '1', 'cms_show_modes', '展现方式', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('47', '栏目第一条内容', '2', 'cms_show_modes', '展现方式', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('48', '发布', '0', 'cms_del_flag', '内容状态', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('49', '删除', '1', 'cms_del_flag', '内容状态', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('50', '审核', '2', 'cms_del_flag', '内容状态', 15, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('51', '首页焦点图', '1', 'cms_posid', '推荐位', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('52', '栏目页文章推荐', '2', 'cms_posid', '推荐位', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('53', '咨询', '1', 'cms_guestbook', '留言板分类', 10, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('54', '建议', '2', 'cms_guestbook', '留言板分类', 20, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('55', '投诉', '3', 'cms_guestbook', '留言板分类', 30, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('56', '其它', '4', 'cms_guestbook', '留言板分类', 40, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('62', '接入日志', '1', 'sys_log_type', '日志类型', 30, '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
insert into SYS_DICT (ID, LABEL, VALUE, TYPE, DESCRIPTION, SORT, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PARENT_ID)
values ('63', '异常日志', '2', 'sys_log_type', '日志类型', 40, '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', null);
commit;
prompt 58 records loaded
prompt Loading SYS_LOG...
prompt Table is empty
prompt Loading SYS_MDICT...
prompt Table is empty
prompt Loading SYS_MENU...
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('1', '0', '0,', '顶级菜单', null, null, null, 0, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('2', '1', '0,1,', '系统设置', null, null, null, 600, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('29-06-2017 10:28:39.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('3', '2', '0,1,2,', '系统设置', null, null, null, 980, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('4', '3', '0,1,2,3,', '菜单管理', '/sys/menu/', null, 'list-alt', 30, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('5', '4', '0,1,2,3,4,', '查看', null, null, null, 30, '0', '0', 'sys:menu:view', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('6', '4', '0,1,2,3,4,', '修改', null, null, null, 30, '0', '0', 'sys:menu:edit', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('7', '3', '0,1,2,3,', '角色管理', '/sys/role/', null, 'lock', 50, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('8', '7', '0,1,2,3,7,', '查看', null, null, null, 30, '0', '0', 'sys:role:view', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('9', '7', '0,1,2,3,7,', '修改', null, null, null, 30, '0', '0', 'sys:role:edit', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('10', '3', '0,1,2,3,', '字典管理', '/sys/dict/', null, 'th-list', 60, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('11', '10', '0,1,2,3,10,', '查看', null, null, null, 30, '0', '0', 'sys:dict:view', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('12', '10', '0,1,2,3,10,', '修改', null, null, null, 30, '0', '0', 'sys:dict:edit', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('13', '2', '0,1,2,', '机构用户', null, null, null, 970, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('29-06-2017 10:20:46.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('14', '13', '0,1,2,13,', '区域管理', '/sys/area/', null, 'th', 50, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('15', '14', '0,1,2,13,14,', '查看', null, null, null, 30, '0', '0', 'sys:area:view', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('16', '14', '0,1,2,13,14,', '修改', null, null, null, 30, '0', '0', 'sys:area:edit', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('17', '13', '0,1,2,13,', '机构管理', '/sys/office/', null, 'th-large', 40, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('18', '17', '0,1,2,13,17,', '查看', null, null, null, 30, '0', '0', 'sys:office:view', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('19', '17', '0,1,2,13,17,', '修改', null, null, null, 30, '0', '0', 'sys:office:edit', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('20', '13', '0,1,2,13,', '用户管理', '/sys/user/', null, 'user', 30, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('21', '20', '0,1,2,13,20,', '查看', null, null, null, 30, '0', '0', 'sys:user:view', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('22', '20', '0,1,2,13,20,', '修改', null, null, null, 30, '0', '0', 'sys:user:edit', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('27', '1', '0,1,', '我的面板', null, null, null, 100, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('28', '27', '0,1,27,', '个人信息', null, null, null, 990, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('29', '28', '0,1,27,28,', '个人信息', '/sys/user/info', null, 'user', 30, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('30', '28', '0,1,27,28,', '修改密码', '/sys/user/modifyPwd', null, 'lock', 40, '1', '0', null, '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('67', '2', '0,1,2,', '日志查询', null, null, null, 985, '1', '0', null, '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('68', '67', '0,1,2,67,', '日志查询', '/sys/log', null, 'pencil', 30, '1', '0', 'sys:log:view', '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('03-06-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('a126620ca7654efc9b76888fd2e4541d', '1', '0,1,', '研发中心', null, null, null, 700, '1', null, null, '1', to_timestamp('29-06-2017 10:24:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('29-06-2017 10:24:56.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('601710f4dcb845038c76d1984ca363c0', 'a126620ca7654efc9b76888fd2e4541d', '0,1,a126620ca7654efc9b76888fd2e4541d,', '业务api', null, null, null, 100, '1', null, null, '1', to_timestamp('29-06-2017 10:25:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('29-06-2017 10:25:33.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
insert into SYS_MENU (ID, PARENT_ID, PARENT_IDS, NAME, HREF, TARGET, ICON, SORT, IS_SHOW, IS_ACTIVITI, PERMISSION, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG)
values ('e3a4cdb009d24db89631e4631c0058ab', '601710f4dcb845038c76d1984ca363c0', '0,1,a126620ca7654efc9b76888fd2e4541d,601710f4dcb845038c76d1984ca363c0,', 'api查看与测试', '/api/index', null, null, 100, '1', null, 'api:view', '1', to_timestamp('29-06-2017 10:26:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('29-06-2017 10:26:44.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0');
commit;
prompt 31 records loaded
prompt Loading SYS_OFFICE...
insert into SYS_OFFICE (ID, PARENT_ID, PARENT_IDS, AREA_ID, CODE, NAME, TYPE, GRADE, ADDRESS, ZIP_CODE, MASTER, PHONE, FAX, EMAIL, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, SORT, PRIMARY_PERSON, DEPUTY_PERSON)
values ('1', '0', '0,', '2', '100000', '广东省总公司', '1', '1', null, null, null, null, null, null, '1', null, '1', null, null, '0', '1', 10, null, null);
insert into SYS_OFFICE (ID, PARENT_ID, PARENT_IDS, AREA_ID, CODE, NAME, TYPE, GRADE, ADDRESS, ZIP_CODE, MASTER, PHONE, FAX, EMAIL, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, SORT, PRIMARY_PERSON, DEPUTY_PERSON)
values ('2', '1', '0,1,', '2', '100001', '公司领导', '2', '1', null, null, null, null, null, null, '1', null, '1', null, null, '0', '1', 10, null, null);
insert into SYS_OFFICE (ID, PARENT_ID, PARENT_IDS, AREA_ID, CODE, NAME, TYPE, GRADE, ADDRESS, ZIP_CODE, MASTER, PHONE, FAX, EMAIL, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, SORT, PRIMARY_PERSON, DEPUTY_PERSON)
values ('3', '1', '0,1,', '2', '100002', '综合部', '2', '1', null, null, null, null, null, null, '1', null, '1', null, null, '0', '1', 20, null, null);
insert into SYS_OFFICE (ID, PARENT_ID, PARENT_IDS, AREA_ID, CODE, NAME, TYPE, GRADE, ADDRESS, ZIP_CODE, MASTER, PHONE, FAX, EMAIL, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, SORT, PRIMARY_PERSON, DEPUTY_PERSON)
values ('4', '1', '0,1,', '2', '100003', '市场部', '2', '1', null, null, null, null, null, null, '1', null, '1', null, null, '0', '1', 30, null, null);
insert into SYS_OFFICE (ID, PARENT_ID, PARENT_IDS, AREA_ID, CODE, NAME, TYPE, GRADE, ADDRESS, ZIP_CODE, MASTER, PHONE, FAX, EMAIL, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, SORT, PRIMARY_PERSON, DEPUTY_PERSON)
values ('5', '1', '0,1,', '2', '100004', '技术部', '2', '1', null, null, null, null, null, null, '1', null, '1', null, null, '0', '1', 40, null, null);
insert into SYS_OFFICE (ID, PARENT_ID, PARENT_IDS, AREA_ID, CODE, NAME, TYPE, GRADE, ADDRESS, ZIP_CODE, MASTER, PHONE, FAX, EMAIL, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, SORT, PRIMARY_PERSON, DEPUTY_PERSON)
values ('7', '1', '0,1,', '3', '200000', '深圳市分公司', '1', '2', null, null, null, null, null, null, '1', null, '1', null, null, '0', '1', 20, null, null);
commit;
prompt 6 records loaded
prompt Loading SYS_ROLE...
insert into SYS_ROLE (ID, OFFICE_ID, NAME, DATA_SCOPE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, IS_SYS, ROLE_TYPE, ENNAME)
values ('1', '1', '系统管理员', '1', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('29-06-2017 10:27:26.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', '1', '1', 'assignment', 'sysrole');
insert into SYS_ROLE (ID, OFFICE_ID, NAME, DATA_SCOPE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, USEABLE, IS_SYS, ROLE_TYPE, ENNAME)
values ('6', '1', '普通用户', '8', '1', to_timestamp('27-05-2013 08:00:00.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), '1', to_timestamp('30-06-2017 18:33:27.000000', 'dd-mm-yyyy hh24:mi:ss.ff'), null, '0', '1', '1', 'assignment', 'commonuser');
commit;
prompt 2 records loaded
prompt Loading SYS_ROLE_MENU...
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '1');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '10');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '11');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '12');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '13');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '14');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '1451bf0f90db4a76865215063948f5ae');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '15');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '16');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '17');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '18');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '19');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '2');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '20');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '21');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '22');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '23');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '24');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '25');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '26');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '27');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '28');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '29');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '3');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '30');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '31');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '32');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '33');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '34');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '35');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '36');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '37');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '38');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '39');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '4');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '40');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '41');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '42');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '43');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '44');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '45');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '46');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '47');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '48');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '49');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '4b614fdac20b4595a9a8af312824b9a4');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '4b7a67c40e25410c97bbac27ecdc762b');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '5');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '50');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '51');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '52');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '53');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '54');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '55');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '56');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '57');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '58');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '59');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '6');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '60');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '601710f4dcb845038c76d1984ca363c0');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '61');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '62');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '63');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '64');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '65');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '66');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '67');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '68');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '69');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '6eb4ca69c66446c59bb1efeda1289dc1');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '7');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '70');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '71');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '72');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '8');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', '9');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', 'a126620ca7654efc9b76888fd2e4541d');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', 'e3a4cdb009d24db89631e4631c0058ab');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('1', 'f357aa511758449caed756ed5aaa1c88');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', '1');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', '27');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', '28');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', '29');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', '30');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', '601710f4dcb845038c76d1984ca363c0');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', 'a126620ca7654efc9b76888fd2e4541d');
insert into SYS_ROLE_MENU (ROLE_ID, MENU_ID)
values ('6', 'e3a4cdb009d24db89631e4631c0058ab');
commit;
prompt 88 records loaded
prompt Loading SYS_ROLE_OFFICE...
prompt Table is empty
prompt Loading SYS_USER...
insert into SYS_USER (ID, COMPANY_ID, OFFICE_ID, LOGIN_NAME, PASSWORD, NO, NAME, EMAIL, PHONE, MOBILE, USER_TYPE, LOGIN_IP, LOGIN_DATE, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE, REMARKS, DEL_FLAG, PHOTO, LOGIN_FLAG)
values ('1', '2', '2', 'admin', '02a3f0772fcca9f415adc990734b45c6f059c7d33ee28362c4852032', '0001', '系统管理员', '54hechuan@163.com', '8675', '8675', null, null, null, '1', null, '1', null, null, '0', null, '1');
commit;
prompt 1 records loaded
prompt Loading SYS_USER_ROLE...
insert into SYS_USER_ROLE (USER_ID, ROLE_ID)
values ('1', '1');
commit;
prompt 1 records loaded
set feedback on
set define on
prompt Done.
