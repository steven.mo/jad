<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/modules/cms/front/include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
	<title>首页</title>
	<meta name="decorator" content="cms_default_${site.theme}"/>
	<meta name="description" content="jad ${site.description}" />
	<meta name="keywords" content="jad ${site.keywords}" />
</head>
<body>

<div class=""  >
<img src="${ctxStatic }/images/front/index.jpg" width="100%" height="100%"/>
    </div>
    
    <div class="hero-unit" style="padding-bottom:35px;margin:10px 0;">
      <c:set var="article" value="${fnc:getArticle('2')}"/>
      <h1>JAD开发框架介绍</h1>
      <p>&nbsp;&nbsp;&nbsp;&nbsp;
      	JAD集成jeesite、dubbo、kisso等多个优秀的开源项目，重构整合而成的高效，高性能的开源Java EE快速开发平台，提供快速的层次清晰的渐进式业务开发模式，功能强大。</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;JAD以Spring Framework为基础框架，以Spring MVC为模型视图控制器，
	在数据访问层上同时支持<strong>hibernaet、MyBatis及spring jpa</strong>，
	以Apache Shiro为权限授权层，以Activit为工作流引擎，在缓存方面，
	它同时支持<strong>Ehcahe、MemCache、Redis</strong>，开发团队可以跟据自身的技术特性，选择自己熟悉的技术组合作为自己的项目架构。</p>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;JAD可用于企业信息化领域，已内置企业信息化系统的基础功能模块，包括：<strong>
	权限组件、数据字典组件、核心工具组件、工作流组件、CMS内容管理、OA办工自动化、微信接口</strong>等。。。
      </p>
      <h2>项目特色</h2>
       <p>1、采用业务与逻辑相分离的架构，具有很强的扩展性与可移值性。</p>
       <p>2、项目架构灵活，不仅支持传统的单体项目架构，也支持基于dubbo的分布式微服务架构。</p>
       <p>3、采用自主研发的Eclipse插件来实现项目创建和代码自动生成。</p>
       <p>4、对常用的ORM框架进行了简单的封装和约定，同时支持hibernate、mybatis、spring jpa，并支持多表关联。</p>
       <p>5、对常见的缓存框架进行简单的封装，同时支持EhCache、MemCache、Redis等缓存引擎。</p>
       <p>6、权限认证方面，同时支持传统的认证方式，也支持可跨域的单点登录。</p>
       <h2>业务功能说明</h2>
       <p>1、基于角色的权限访问控制模块。采用流行的shiro实现在基于角色的权限访问，包括会员管理、角色管理、菜单管理、权限管理等子模块。</p>
       <p>2、自带cms内容管理模块。</p>
       <p>3、集成activiti，提供了工作流系统，作为oa办工自动化模块的流程引擎（开发中。。。）</p>
       <p>4、本项目实现了微信接口（开发中。。。）</p>
       <p>以上各个业务模块之间相互独立，开发人员可以跟据自己的需要任意选择自己的想要的模块。并且可以以传统的单体项目架构方式集成各个个模块，也可以以微服务的方式分布式部署各个模块。</p>
       
       <h2>逻辑功能说明</h2>
       <p>1、提供可高度定制化的Eclipse插件来自动创建和配置项目及自动生成CURD业务代码。</p>
       <p>2、可自动生成业务api，方便前后端连调与测试。</p>
       <p>3、同时支持EhCache、MemCache、Redis等缓存引擎。</p>
       <p>4、权限认证方面，同时支持传统的认证方式，也支持可跨域的单点登录。</p>
       <p>5、集成dubbo，可用分布式微服务架构部署项目。</p>
       <p>6、提供应用层waf防火墙，防止sql注入及xss脚本攻击。</p>
       <p>7、提供测试框架。</p>
       <p>8、提供常用工具类。</p>
       <p>9、提供项目Demo以及快速生成项目的向导。</p>
       
       
      <p><a href="${ctx}/view-5ae877a29684409da3b45de4c7a2bb6f-d537e2e504e54b9cb85244b31d41836f.html" class="btn btn-primary btn-large">&nbsp;&nbsp;&nbsp;查看详情 &raquo;&nbsp;&nbsp;&nbsp;</a></p>
    </div>
    <div class="row">
      <div class="span4">
        <h4><small><a href="${ctx}/list-5ae877a29684409da3b45de4c7a2bb6f${urlSuffix}" class="pull-right">更多&gt;&gt;</a></small>项目介绍</h4>
		<ul><c:forEach items="${fnc:getArticleList2(site.id, '5ae877a29684409da3b45de4c7a2bb6f', 8, '',contextPath,frontPath,urlSuffix)}" var="article">
			<li><span class="pull-right"><fmt:formatDate value="${article.updateDate}" pattern="yyyy.MM.dd"/></span>
			<a href="${article.url}" target="_blank" style="color:${article.color}">${fns:abbr(article.title,38)}</a></li>
		</c:forEach></ul>
      </div>
      <div class="span4">
        <h4> <small><a href="${ctx}/list-41e72926000c47189d69cbd5fb1f722b${urlSuffix}" class="pull-right">更多&gt;&gt;</a></small>项目安装与业务开发</h4>
		<ul><c:forEach items="${fnc:getArticleList(site.id, '41e72926000c47189d69cbd5fb1f722b', 8, '')}" var="article">
			<li><span class="pull-right"><fmt:formatDate value="${article.updateDate}" pattern="yyyy.MM.dd"/></span>
			<a href="${article.url}" target="_blank" style="color:${article.color}">${fns:abbr(article.title,38)}</a></li>
		</c:forEach></ul>
      </div>
      <div class="span4">
        <h4><small><a href="${ctx}/list-ba19b9772c0243dea6ce53f1627cba9a${urlSuffix}" class="pull-right">更多&gt;&gt;</a></small>源码分析</h4>
		<ul><c:forEach items="${fnc:getArticleList(site.id, 'ba19b9772c0243dea6ce53f1627cba9a', 8, '')}" var="article">
			<li><span class="pull-right"><fmt:formatDate value="${article.updateDate}" pattern="yyyy.MM.dd"/></span>
			<a href="${article.url}" target="_blank" style="color:${article.color}">${fns:abbr(article.title,38)}</a></li>
		</c:forEach></ul>
      </div>
    </div>
</body>
</html>